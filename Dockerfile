# 実行環境の JDK バージョンにあわせてビルドバージョンを選択します。
FROM amazoncorretto:17 AS build

# ビルドツールのインストール
RUN curl -Lo /bin/lein https://raw.githubusercontent.com/technomancy/leiningen/stable/bin/lein && chmod +x /bin/lein

# ソースを作業ディレクトリにコピー
WORKDIR /app
COPY resources resources
COPY src src
COPY config config
COPY project.clj .

# ビルド
RUN lein uberjar


# 成果物をイメージから取り出すための実行環境を作成します。
FROM busybox AS dist
COPY --from=build /app/target/student-0.0.1-SNAPSHOT-standalone.jar /server.jar
CMD ["cp", "/server.jar", "/data/"]
