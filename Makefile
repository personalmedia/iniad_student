IMAGE_NAME=student

.PHONY: help
help:                   # このヘルプ
	@egrep '^[a-zA-Z][-.a-zA-Z0-9 ]*:' Makefile


.PHONY: build
build:			# ビルド
	lein uberjar

install: build		# ビルドしてインストール
	scp target/student-0.0.1-SNAPSHOT-standalone.jar iniad-api:/opt/iniad_student/app.jar


restart:		# リモートサーバを再起動
	ssh iniad-api sudo systemctl restart student


log:			# リモートサーバのログを取得 (20 日)
	@ssh iniad-api journalctl -lu student --since=-1728000
log.90:			# リモートサーバのログを取得 (90 日)
	@ssh iniad-api journalctl -lu student --since=-7776000

fmt:			# ソースのフォーマット
	@cljstyle fix


.PHONY: test
test:			# テストの実行
	@lein test


outdated:		# ライブラリの更新チェック
	@lein ancient || true


security-check:		# セキュリティチェック
	@(cd nvd; lein with-profile -user run -m nvd.task.check "" "$$(cd ..; lein with-profile -user,-dev classpath)") || true


uberjar:		# Docker コンテナで jar ビルド
	@docker build --pull -t $(IMAGE_NAME) .
	@docker run --rm -v $(PWD):/data $(IMAGE_NAME)
	@docker rmi $(IMAGE_NAME)
