# データモデル

部屋のデバイスに固有の番号を付けて識別するためのデータモデル。


## APIから

/doorlocks/:room_num/1

ある部屋の番号1の電気錠を意味する。

注意点としては、電気錠はとなりの部屋からも同様に見える場合があるため、
ひとつの電気錠は複数のエンドポイントを持つこと。
つまり、 /doorlocks/1000/1  /doorlocks/2000/1 が同じucodeということがありうる。

電気錠、照明、空調といったデバイスのタイプごとに一意番号を振る。
つまり、 /doorlocks/1000/1  /aircons/1000/1 はありうる。

## モデル定義

* デバイス(devices)
    * device_type text (aircon, doorlock, light)
    * room_no int (4桁の数字による部屋番号,  0 < x < 10000)
    * item_no int (数字による部屋内でのデバイス一意識別, 0 < x < 1000)
    * ucode text (デバイスのucode)

    * ユニーク制約
        * (device_type, room_no, item_no)
        * (device_type, room_no, ucode)

```
create table api.devices (
ucode text not null,
device_type text not null,
room_no int not null constraint valid_room_no check (0 < room_no AND room_no < 10000),
item_no int not null constraint valid_item_no check (0 < item_no and item_no < 1000),
primary key (device_type,room_no,item_no),
unique (device_type,room_no,ucode));
```

```
edu=# grant select on api.devices to web_anon;
edu=# grant all on api.devices to edu_user;
```
