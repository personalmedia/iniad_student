(defproject student "0.0.1-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.12.0"]
                 [org.clojure/core.async "1.6.681"]
                 [org.clojure/core.cache "1.1.234"]
                 [org.clojure/data.json "2.5.0"]
                 [org.clojure/core.match "1.1.0"]

                 [io.pedestal/pedestal.service "0.6.4"
                  :exclusions [cheshire
                               org.clojure/tools.reader]]

                 ;; Remote Debug Support
                 [nrepl "1.3.0"]
                 [cider/cider-nrepl "0.50.2"]

                 ;; Database
                 [com.github.seancorfield/next.jdbc "1.3.939"]
                 [org.postgresql/postgresql "42.7.4"]
                 [com.zaxxer/HikariCP "5.1.0"
                  :exclusions [org.slf4j/slf4j-api]]

                 ;; OpenCV
                 [origami "4.5.0"
                  :exclusions [org.slf4j/slf4j-nop]]

                 ;; App deps.
                 [diehard "0.11.12"]
                 [http-kit "2.8.0"]
                 [clj-time "0.15.2"]
                 [jerks-whistling-tunes "0.3.2"
                  :exclusions [org.bouncycastle/bcprov-jdk15on]]
                 [environ "1.2.0"]
                 [buddy/buddy-core "1.12.0-430"]

                 [commons-io "2.16.1"]
                 [commons-fileupload "1.5"]

                 ;; Websocket Client
                 [stylefruits/gniazdo "1.2.2"]
                 [org.eclipse.jetty.websocket/websocket-client
                  ;; io.pedestal/pedestal.jetty の内部依存とバージョンをあわせる
                  "9.4.54.v20240208"]

                 ;; Remove this line and uncomment one of the next lines to
                 ;; use Immutant or Tomcat instead of Jetty:
                 [io.pedestal/pedestal.jetty "0.6.4"
                  ;; :exclusions [org.eclipse.jetty.websocket/websocket-api
                  ;;             org.eclipse.jetty.websocket/websocket-server
                  ;;             org.eclipse.jetty.websocket/websocket-servlet]
                  ]
                 ;; [io.pedestal/pedestal.immutant "0.5.10"]
                 ;; [io.pedestal/pedestal.tomcat "0.5.10"]

                 [ch.qos.logback/logback-classic "1.5.8"
                  :exclusions [org.slf4j/slf4j-api]]
                 [org.slf4j/jul-to-slf4j "2.0.16"]
                 [org.slf4j/jcl-over-slf4j "2.0.16"]
                 [org.slf4j/log4j-over-slf4j "2.0.16"]]
  :plugins [[lein-ancient "1.0.0-RC3"]
            [lein-environ "1.2.0"]]
  :min-lein-version "2.4.0"
  :resource-paths ["config", "resources"]

  ;; 速度低下の原因になる実行時型チェックを使用しているとき、警告を出す。
  :global-vars {*warn-on-reflection* true}

  ;; If you use HTTP/2 or ALPN, use the java-agent to pull in the correct alpn-boot dependency
  ;; :java-agents [[org.mortbay.jetty.alpn/jetty-alpn-agent "2.0.6"]]
  :profiles {:dev {:aliases {"run-dev" ["trampoline" "run" "-m" "student.server/run-dev"]}
                   :dependencies [#_[io.pedestal/pedestal.service-tools "0.5.10"]]}
             :uberjar {:aot [student.server]}}
  :main ^{:skip-aot true} student.server)
