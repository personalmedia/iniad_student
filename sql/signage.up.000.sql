-- サイネージ本体
CREATE TABLE IF NOT EXISTS api.signage
( signage_no    text       NOT NULL
, acc_read      text []    NOT NULL DEFAULT '{}'
, acc_write     text []    NOT NULL DEFAULT '{}'
, PRIMARY KEY   (signage_no)
, CONSTRAINT    signage_no CHECK (signage_no ~ '^[0-9][0-9][0-9][0-9]$')
);
GRANT ALL ON TABLE api.signage TO web_anon;

-- サイネージに登録された画像情報
CREATE TABLE IF NOT EXISTS api.signage_image
( signage_no    text       NOT NULL
, item_no       integer    NOT NULL
, title         text       NOT NULL
, playtime      integer    NOT NULL
, created_by    text       NOT NULL
, created_at    timestamptz NOT NULL DEFAULT now()
, PRIMARY KEY   (signage_no, item_no)
, FOREIGN KEY   (signage_no) REFERENCES api.signage(signage_no)
  ON UPDATE CASCADE
  ON DELETE CASCADE
, CONSTRAINT    item_no CHECK (0 < item_no)
, CONSTRAINT    title CHECK (length(title) <= 1024)
);
GRANT ALL ON TABLE api.signage_image TO web_anon;

-- サイネージに登録された画像のイメージデータ
CREATE TABLE IF NOT EXISTS api.signage_image_data
( signage_no    text       NOT NULL
, item_no       integer    NOT NULL
, content       bytea      NOT NULL
, created_by    text       NOT NULL
, created_at    timestamptz NOT NULL DEFAULT now()
, PRIMARY KEY   (signage_no, item_no)
, FOREIGN KEY   (signage_no, item_no) REFERENCES api.signage_image(signage_no, item_no)
  ON UPDATE CASCADE
  ON DELETE CASCADE
);
GRANT ALL ON TABLE api.signage_image_data TO web_anon;

-- サイネージのアクセス権を変更できる特権グループ
CREATE TABLE IF NOT EXISTS api.signage_priv
( priv_name     text       NOT NULL -- 特権グループ名
, groups        text []    NOT NULL -- 所属する権限グループ名
, PRIMARY KEY   (priv_name)
);
GRANT ALL ON TABLE api.signage_priv TO web_anon;
