(ns student.keycloak
  (:require
   [buddy.core.codecs :as codecs]
   [clojure.string :as str]
   [diehard.core :refer [defretrypolicy with-retry]]
   [environ.core :refer [env]]
   [io.pedestal.log :as log]
   [org.httpkit.client :as h]
   [student.auth-config :as auth]
   [student.util :as util]))


(def default-opts
  {:client_id "admin-cli"
   :scope     "openid profile email"})


(def ^:private admin-secrets
  {:username "felica-api-user"
   :password "OTh4je6choh7vahh"})


(def ^:private access-token-leeway
  "トークンの有効期限切れより何秒手前で切れたと判断するかの秒数"
  (util/string->int (env :access-token-leeway) :default 300))


(def ^:private ctx (atom {}))


(defretrypolicy by-status
  {:max-duration-ms 5000
   :delay-ms        250

   :retry-if   (fn [return-value exception-thrown]
                 (or exception-thrown
                     (nil? (:status return-value))
                     (some #(= % (:status return-value))
                           [500 502 503 504])))
   :on-failure (fn [result exception]
                 (log/error :retry :failure :result result :exception exception))
   :on-abort   (fn [result exception]
                 (log/error :retry :abort :result result :exception exception))})


(defn ->qualified-map
  [ns m]
  (update-keys m #(keyword ns (name %))))


(defn decode-jwt-unverify
  [s]
  (try
    (let [[_ s _] (str/split s #"\.")]
      (when s
        (-> s
            codecs/str->bytes
            codecs/b64u->bytes
            codecs/bytes->str
            util/json-read-str)))
    (catch Exception ex (log/error :ex ex))))


(defn- enrich-context
  [m]
  (when-let [id_token (:id_token m)]
    (->qualified-map "id_token" (decode-jwt-unverify id_token))))


(defn- decode-token-response
  [ctx {:keys [status body]}]
  (let [body (util/json-read-str body)
        body (conj body (enrich-context body))]
    (if (:access_token body)
      (reset! ctx body)
      (log/error :status status :body body))
    {:status status
     :body   body}))


(defn- login
  [ctx {:as opts}]
  @(h/post
    (:token_endpoint @auth/openid-configuration)
    {:basic-auth  [(:client_id auth/oidc) (:client_secret auth/oidc)]
     :form-params (conj default-opts {:grant_type "password"} opts)}
    (partial decode-token-response ctx)))


(defn logout
  [ctx {:as opts}]
  @(h/post
    (:end_session_endpoint @auth/openid-configuration)
    {:basic-auth  [(:client_id auth/oidc) (:client_secret auth/oidc)]
     :form-params (conj default-opts {:refresh_token (:refresh_token @ctx)} opts)}
    (partial decode-token-response ctx)))


(defn- refresh
  [ctx {:as opts}]
  (if-let [refresh-token (:refresh_token @ctx)]
    (let [{:keys [status] :as res}
          @(h/post
            (:token_endpoint @auth/openid-configuration)
            {:basic-auth  [(:client_id auth/oidc) (:client_secret auth/oidc)]
             :form-params (conj default-opts
                                {:grant_type    "refresh_token"
                                 :refresh_token refresh-token}
                                opts)}
            (partial decode-token-response ctx))]
      (if (= status 400)
        ;; Refresh トークンの期限切れの可能性があるので、ログインし直す
        (login ctx opts)
        res))
    (login ctx opts)))


(defn- userinfo
  [token]
  (with-retry {:policy by-status}
    @(h/get
      (:userinfo_endpoint @auth/openid-configuration)
      {:oauth-token token}
      (fn [{:keys [status body]}]
        (let [body (util/json-read-str body)]
          {:status status
           :body   body})))))


(defn introspection
  "JWTトークンの検証をおこなう"
  [token]
  (with-retry {:policy by-status}
    @(h/post
      (:introspection_endpoint @auth/openid-configuration)
      {:basic-auth  [(:client_id auth/oidc) (:client_secret auth/oidc)]
       :form-params {:token token}}
      (fn [{:keys [status body]}]
        (let [body (util/json-read-str body)]
          {:status status
           :body   body})))))


(defn- has-enough-time
  "トークンの有効期限まで十分な時間が残っている場合 true"
  [tokens]
  (let [lwy access-token-leeway
        iat (or (:id_token/iat tokens) 0)
        exp (or (:expires_in tokens) 0)
        lim (+ iat (max (- exp lwy) 0))
        now (util/epoch-now)]
    (log/trace :iat iat
               :exp exp
               :lim lim
               :now now
               :ok  (< now lim))
    (< now lim)))


(defn- append-userinfo
  [{:keys [status body] :as response}]
  (if (and (= status 200)
           (:access_token body))
    (let [userinfo (userinfo (:access_token body))]
      (if (and (= (:status userinfo) 200)
               (get-in userinfo [:body :sub]))
        (update response :body
                #(conj % (->qualified-map "userinfo" (:body userinfo))))
        response))
    response))


(defn get-access-token
  ([{:as opts}]
   (get-access-token (atom {}) opts))
  ([ctx {:as opts}]
   (let [tokens @ctx]
     (if-let [access-token (:access_token tokens)]
       ;; すでにアクセストークンを持っている場合、 userinfo で有効性を再確認する
       (let [userinfo (userinfo access-token)]
         (if (get-in userinfo [:body :sub])
           (if (has-enough-time tokens)
             {:status 200
              :body   (conj tokens
                            (->qualified-map "userinfo" (:body userinfo)))}
             ;; 有効期限切れなので、リフレッシュする
             (-> (with-retry {:policy by-status} (refresh ctx opts))
                 append-userinfo))
           ;; トークン検証に失敗したので、リフレッシュする
           (if (= (:status userinfo) 401)
             (-> (with-retry {:policy by-status} (refresh ctx opts))
                 append-userinfo)
             (do (log/error :invalid-token access-token
                            :userinfo userinfo)
                 userinfo))))
       ;; アクセストークンを持っていない場合、新規に取得する
       (-> (with-retry {:policy by-status}
             (login ctx opts))
           append-userinfo)))))


(defn access-token
  "Keycloak Admin REST API にアクセスするためのトークンを返す"
  []
  (-> (get-access-token ctx admin-secrets)
      (get-in [:body :access_token])))
