(ns student.database
  (:require
   [diehard.core :refer [defretrypolicy with-retry]]
   [io.pedestal.log :as log]
   [next.jdbc :as jdbc]
   [next.jdbc.connection :as jdbc.connection]
   [next.jdbc.result-set :as jdbc.rs])
  (:import
   (com.zaxxer.hikari
    HikariDataSource)))


(def db
  {:dbtype   "postgres"
   :dbname   "ubiqdata"
   :user     "portaladmin"
   :password "MwZG17RbcG4nSn9u"
   :host     "127.0.0.1"
   :port     5432})


(def ^:private ds
  (jdbc.connection/->pool
   HikariDataSource
   (conj db {:username             (:user db)
             :maximumPoolSize      2
             :dataSourceProperties {:socketTimeout 20}})))


(defretrypolicy retry-policy
  {:max-duration-ms 5000
   :delay-ms        250

   :retry-on   Exception
   :on-failure (fn [result exception]
                 (log/error :retry :failure :result result :exception exception))
   :on-abort   (fn [result exception]
                 (log/error :retry :abort :result result :exception exception))})


(defn ->int-like
  [v]
  (cond
    (number? v) v
    (string? v) (parse-long v)
    :else       nil))


(defn lockers
  [& {:keys [user type]
      :or   {type 1}}]
  (try
    (with-retry {:policy retry-policy}
      (jdbc/execute-one!
       ds
       ["
SELECT userid, groupid, boxno, adr, typ
FROM acl_lockers
WHERE userid = ?
  AND typ = ?
  AND adr IS NOT NULL
ORDER BY adr
LIMIT 1
"
        user
        (->int-like type)]
       {:builder-fn jdbc.rs/as-unqualified-maps}))
    (catch Exception e
      (log/error :message (ex-message e)
                 :data (ex-data e)))))
