(ns student.ubiq
  (:require
   [clojure.core.async :as a
    :refer [>!! chan timeout close! alts!!]]
   [clojure.core.match :refer [match]]
   [clojure.set :as set]
   [clojure.string :as str]
   [environ.core :refer [env]]
   [gniazdo.core :as ws]
   [io.pedestal.log :as log]
   [org.httpkit.client :as h]
   [student.auth :as auth]
   [student.database :as database]
   [student.keycloak-felica :as felica]
   [student.util :as util])
  (:import
   java.math.RoundingMode))


(def ubiq-authority "https://ubiq-serv.iniad.org")

(def nb-authority "wss://portal.iniad.org:9201/")


(def pgrst-authority
  "PostgREST サーバのベースURL

  無指定時はlocalhostで動作しているものとする。"
  (env :pgrst-url "http://localhost:5430"))


(def signage-url
  "サイネージAPIのURL"
  (str pgrst-authority "/signage"))


(def whiteips-url
  "接続許可IPアドレスリストAPIのURL"
  (str pgrst-authority "/whiteips"))


(def device-map-url
  "デバイスucode番号マッピングAPIのURL"
  (str pgrst-authority "/devices"))


(def device-map-token
  "デバイスマップ書き換え用認証トークン"
  (env :device-map-token
       "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiZWR1X3VzZXIifQ.KcrtHxQb2WHi4B_NtUBhW3Ofs02yTx62_4NxcrblWXg"))


(defn nb-get-aircon
  "空調の現在状態を取得する

  HTTP APIの場合BACnetに直接問い合わせが発生し、それが遅いため、
  NB経由で問い合わせを実施する。NBがキャッシュしているため早い。

  access-token 問い合わせの主体ユーザのOpenIdアクセストークン
  ucode 空調のucode"
  [access-token ucode & {:keys [add]}]
  (let [recv   (chan)
        sock   (ws/connect nb-authority
                           :subprotocols ["wamp.2.json"]
                           :on-receive #(>!! recv (util/json-read-str %))
                           :on-connect (fn [_sess] (>!! recv true))
                           :on-close (fn [_code _reason] (>!! recv false)))
        send   (fn [msg]
                 (->> msg
                      util/json-write-str
                      (ws/send-msg sock)))
        result (match [(alts!! [recv (timeout 5000)])]
                 [[true _]]
                 (do
                   (send
                    [1, (str "javascript." access-token), {:roles {}}])
                   (match [(alts!! [recv (timeout 5000)])]
                     [[[2 _ _] _]]
                     (do
                       (send [32, 0, {},
                              (str "get.devices.aircons." ucode)])
                       (match [(alts!! [recv (timeout 5000)])]
                         [[[33 _ _] _]]
                         (match [(alts!! [recv (timeout 5000)])]
                           [[[36 _ _ _ [body]] _]]
                           (some-> body
                                   (conj body add))
                           :else nil)
                         :else nil))
                     :else nil))
                 :else nil)]
    (close! recv)
    (ws/close sock)
    result))


(comment
  ;;(nb-get-aircon at "00001C000000000000020000004804D2")
  ;;=> {:on false, :mode "cool", :temperature 25.0, :wind "low"}
  )


(defn search-devices
  [access-token place & {:keys [class full symbol proxy-as]
                         :or   {class  "sensor"
                                full   true
                                symbol true}}]
  (some-> @(h/get
            (str ubiq-authority "/devices/search")
            {:oauth-token  access-token
             :query-params (-> {:class     class
                                :full      full
                                :symbolize symbol}
                               (conj (when place {:place place}))
                               (conj (when proxy-as
                                       {:proxy_user (:user proxy-as)
                                        :proxy_time (:time proxy-as)})))})
          (as-> res (if (= (:status res) 200) (:body res)
                        (log/error :search-devices res)))
          (util/json-read-str)))


(defn send-proxy-request
  [access-token path method query json]
  (letfn [(into-response
            [res]
            (condp = (:status res)
              404 {:status 404
                   :body   {:status      "error",
                            :description "Not found"}}
              {:status  (:status res)
               :headers {"Content-Type" (get-in res [:headers :content-type])}
               :body    (:body res)}))]
    (case method
      :get  (some->
             @(h/get
               (str ubiq-authority "/" path)
               {:oauth-token  access-token
                :query-params query})
             into-response)
      :post (some->
             @(h/post
               (str ubiq-authority "/" path)
               {:oauth-token  access-token
                :query-params query
                :json-params  json})
             into-response)
      :put  (some->
             @(h/put
               (str ubiq-authority "/" path)
               {:oauth-token  access-token
                :query-params query
                :json-params  json})
             into-response))))


(defn get-place-profile
  [access-token place-id]
  (some-> @(h/get
            (str ubiq-authority "/places/" place-id "/profile")
            {:oauth-token access-token})
          (as-> res (when (= (:status res) 200) (:body res)))
          (util/json-read-str)))


(defn get-room-devices
  "部屋に属するデバイスの一覧を取得する"
  [access-token & {:keys [class]}]
  (some->> (search-devices access-token nil
                           :symbol true
                           :full true
                           :class class)
           ;; places に有効な部屋を含むもののみ抽出
           (filter (fn [d]
                     (->> (get-in d [:position :places])
                          (some #(and (string? %)
                                      (or (str/starts-with? % "room")
                                          (= % "rooftop")))))))
           ;; データを変形
           (map (fn [d]
                  (-> d
                      (select-keys [:id :name :symbol])
                      (conj {:item   (first (:classes d))
                             :places (filter
                                      (fn [p] (not (= "hub1" p)))
                                      (get-in d [:position :places]))})
                      ;; 複数の部屋を部屋ごとに分離
                      (as-> d
                        (map (fn [p]
                               (-> d
                                   (dissoc :places)
                                   (conj {:place p})))
                             (:places d))))))
           seq
           flatten
           (group-by :place)
           vals
           (map (fn [xs]
                  (->> xs
                       (sort-by :symbol)
                       (map-indexed (fn [idx x] (conj x {:idx idx}))))))
           flatten))


(defn get-room-devices-to-string
  [access-token]
  (let [place-no (fn [p]
                   (cond
                     (str/starts-with? p "room") (subs p 4)
                     (= p "rooftop")             "6000"
                     :else                       p))
        doorlock (get-room-devices access-token :class "doorlock")
        aircon   (get-room-devices access-token :class "aircon")
        light    (get-room-devices access-token :class "light")
        joined   (->> (flatten [doorlock aircon light])
                      (filter identity)
                      (map
                       (fn [x]
                         (str (:id x) ","
                              (:item x) ","
                              (place-no (:place x)) ","
                              (+ 1 (:idx x)) "\n")))
                      (str/join))]
    (str "ucode,device_type,room_no,item_no\n"
         joined)))


(defn get-device-profile
  [access-token device-id & {:keys [proxy-as]}]
  (some-> @(h/get
            (str ubiq-authority "/devices/" device-id "/profile")
            {:oauth-token access-token
             :query-params
             (when proxy-as
               {:proxy_user (:user proxy-as)
                :proxy_time (:time proxy-as)})})
          (as-> res
            (if (= (:status res) 200) (:body res)
                (log/error :get-device-profile (keep res [:status :body]))))
          (util/json-read-str)))


(defn get-device-status
  [access-token device-id
   & {:keys [class add] :or {class "sensor"}}]
  (some-> @(h/get
            (str ubiq-authority "/" class "s/" device-id "/status")
            {:oauth-token access-token})
          (as-> res (when (= (:status res) 200) (:body res)))
          (util/json-read-str)
          (dissoc :time)
          (conj add)
          (clojure.set/rename-keys {:pressure :airpressure})))


(defn set-device-status
  [access-token device-id value
   & {:keys [class add]}]
  (some-> @(h/put
            (str ubiq-authority "/" class "s/" device-id "/status")
            {:oauth-token access-token
             :headers     util/content-type-json
             :body        (util/json-write-str value)})
          (as-> res (when (= (:status res) 200) (:body res)))
          (util/json-read-str)
          (dissoc :time)
          (conj add)
          (clojure.set/rename-keys {:pressure :airpressure})))


(defn set-doorlock-status
  [access-token device-id lockp
   & {:keys [add]}]
  (some-> @(h/post
            (str ubiq-authority "/doorlocks/" device-id "/"
                 (if lockp "lock" "unlock"))
            {:oauth-token access-token
             :headers     util/content-type-json
             :body        (util/json-write-str {})})
          (as-> res (when (= (:status res) 200) (:body res)))
          (util/json-read-str)
          (conj add)))


(defn average
  [maps]
  (let [two (BigDecimal. 2)
        num #(if (not= (class %) BigDecimal) (BigDecimal. (double %)) %)]
    (->> (reduce
          (fn [x y]
            (merge-with (fn [a b]
                          (.divide ^BigDecimal (+ (num a) (num b)) two
                                   4 RoundingMode/HALF_EVEN))
                        x y))
          maps)
         (reduce-kv
          #(assoc %1 %2 (.setScale ^BigDecimal (num %3)
                                   ;; 放射線だけ例外で 3桁 まで有効とする
                                   (case %2 :radiation 3 1)
                                   RoundingMode/HALF_EVEN))
          {}))))


(defn light-modes
  [props]
  (some-> props
          :symbol
          (as-> sym
            (when (str/starts-with? sym "LP")
              (-> (subs sym 2)
                  seq
                  (as-> xs
                    (reduce (fn [acc x]
                              (case x
                                \F (conj acc "full")
                                \D (conj acc "dim")
                                \N (conj acc "nowindowside")
                                \O (conj acc "off")
                                \S (vec
                                    (concat acc ["lv6" "lv5" "lv4"
                                                 "lv3" "lv2" "lv1"]))
                                acc))
                            []
                            xs)))))))


(defn replace-light-mode
  [props]
  (cond
    (contains? props :pattern) (clojure.set/rename-keys props {:pattern :mode})
    (contains? props :on)      (let [mode (:on props)]
                                 (-> props
                                     (dissoc :on)
                                     (assoc :mode (if mode "full" "off"))))
    :else                      props))


(defn renumber-device
  [class room-no item-no to]
  (let [params {:select      "device_type,room_no,item_no"
                :device_type (str "eq." class)
                :room_no     (str "eq." room-no)
                :item_no     (str "eq." item-no)
                :order       "item_no"}
        value  {:item_no to}]
    (some-> @(h/patch
              (str device-map-url)
              {:oauth-token  device-map-token
               :headers      (conj util/content-type-json
                                   {"Prefer" "return=representation"})
               :query-params params
               :body         (util/json-write-str value)})
            (as-> res (when (= (:status res) 200) (:body res)))
            (util/json-read-str)
            (seq))))


(defn get-accessibility-in-room
  [access-token room-no & {:keys [proxy-as]}]
  (let [place   (case (long room-no)
                  6000 "00001C00000000000002000000480A9C"
                  (str "room" room-no))
        sensors (some-> (search-devices access-token place
                                        :class "sensor"
                                        :proxy-as proxy-as)
                        (seq)
                        (->> (map #(-> %
                                       (clojure.set/rename-keys {:id :ucode})
                                       (assoc :device_type "sensor"
                                              :room_num (str room-no)
                                              :item_num nil
                                              :proxy_user (:user proxy-as)
                                              :proxy_time (:time proxy-as))
                                       (dissoc :classes)
                                       (dissoc :position)))))
        devices (some->
                 @(h/get (str device-map-url)
                         {:query-params
                          {:select  "ucode,device_type,room_no,item_no"
                           :room_no (str "eq." room-no)
                           :order   "device_type,item_no"}})
                 (as-> res (when (= (:status res) 200) (:body res)))
                 (util/json-read-str)
                 (seq)
                 (->> (map #(-> (assoc %
                                       :prof (get-device-profile
                                              access-token (:ucode %)
                                              :proxy-as proxy-as)))))
                 (as-> xs
                   (filter #(:prof %) xs))
                 (seq)
                 (->> (map #(-> %
                                (as-> x
                                  (assoc
                                   x
                                   :symbol (get-in x [:prof :symbol])
                                   :name (get-in x [:prof :name])
                                   :accessible (get-in x [:prof :accessible])
                                   :room_num (str (:room_no %))
                                   :item_num (:item_no %)
                                   :proxy_user (:user proxy-as)
                                   :proxy_time (:time proxy-as)))
                                (dissoc :prof)
                                (dissoc :room_no)
                                (dissoc :item_no)))))]
    (->> (concat sensors devices)
         (filter identity))))


(defn ip-is-allow?
  "文字列表現のIPアドレス ipstr がアクセス許可リストにある場合のみ true"
  [ipstr]
  (let [params {:ip (str "eq." ipstr)}]
    (-> @(h/get whiteips-url {:headers      {"accept" "application/vnd.pgrst.object+json"}
                              :query-params params})
        :status
        (= 200))))


(defn get-devices-in-room
  [access-token place & {:keys [class f item-no]
                         :or   {f identity}}]
  (let [place-no   (fn [p]
                     (if (clojure.string/starts-with? p "room")
                       (subs p 4)
                       "6000"))
        accessible (fn [id]
                     (some-> (get-device-profile access-token id)
                             :accessible))
        params     (-> {:select      "ucode,room_no,item_no"
                        :device_type (str "eq." class)
                        :room_no     (str "eq." (place-no place))
                        :order       "item_no"}
                       (as-> p
                         (if item-no
                           (conj p {:item_no (str "eq." item-no)})
                           p)))]
    (some-> @(h/get (str device-map-url) {:query-params params})
            (as-> res (if (= (:status res) 200) (:body res)
                          (log/error :device-map res)))
            (util/json-read-str)
            (seq)
            (->> (map (fn [x] (conj x {:accessible (accessible (:ucode x))}))))
            (as-> xs
              {:accessible (seq (map f (filter :accessible xs)))}))))


(defn get-light
  [access-token device-sym]
  (let [class  "light"
        prof   (get-device-profile access-token device-sym)
        device (get-device-status
                access-token device-sym
                :class class
                :add {:modes (or (light-modes prof)
                                 ["full" "off"])})]
    (replace-light-mode device)))


(defn set-light
  [access-token device-sym value]
  (let [class  "light"
        prof   (get-device-profile access-token device-sym)
        modes  (light-modes prof)
        modes' (or modes ["full" "off"])
        valid? (some #(= value %) modes')]
    (if valid?
      (let [device (set-device-status
                    access-token device-sym
                    (if modes
                      {:pattern value}
                      {:on (= "full" value)})
                    :class class
                    :add {:modes modes'})]
        (replace-light-mode device))
      nil)))


(defn set-doorlock
  [access-token device-sym value]
  (let [_class "doorlock"]
    (if (boolean? value)
      (let [device (set-doorlock-status access-token device-sym value)]
        device)
      nil)))


(defn set-aircon
  [access-token device-sym value]
  (let [class  "aircon"
        device (set-device-status access-token device-sym value :class class)]
    device))


(defn get-device
  [access-token device-sym class]
  (get-device-status access-token device-sym
                     :class class))


(defn get-doorlock
  [access-token device-sym]
  (get-device access-token device-sym "doorlock"))


(defn aircon-model-symbol
  [device-sym]
  (let [model (try (subs device-sym 0 3)
                   (catch Exception _ nil))]
    (when (some #(= model %) ["DEX" "MHU" "OAP" "OAV" "PAC" "VAM" "VEN" "VRV"])
      model)))


(defn fix-aircon-model-value
  [model v]
  (-> (case model
        "DEX" {:modes             ["auto" "cool" "heat" "fan"]
               :winds             ["auto" "low" "middle" "high"]
               :temperature_range [15, 30]
               :humidity_range    [30, 80]}
        "MHU" {:modes             nil
               :winds             nil
               :temperature_range nil
               :humidity_range    nil}
        "OAP" {:modes             ["auto" "cool" "heat" "fan"]
               :winds             nil
               :temperature_range nil
               :humidity_range    nil}
        "OAV" {:modes             ["auto" "cool" "heat" "fan"]
               :winds             nil
               :temperature_range nil
               :humidity_range    nil}
        "PAC" {:modes             ["auto" "cool" "heat" "fan" "dry"]
               :winds             ["auto" "low" "high"]
               :temperature_range [15, 30]
               :humidity_range    nil}
        "VAM" {:modes             ["auto" "normal" "total"]
               :winds             ["auto" "low" "high"
                                   "auto_freshup"
                                   "low_freshup"
                                   "high_freshup"]
               :temperature_range nil
               :humidity_range    nil}
        "VEN" {:modes             nil
               :winds             nil
               :temperature_range nil
               :humidity_range    nil}
        "VRV" {:modes             ["auto" "cool" "heat" "fan" "dry"]
               :winds             ["auto" "low" "high"]
               :temperature_range [15, 30]
               :humidity_range    nil}
        {:modes             nil
         :winds             nil
         :temperature_range nil
         :humidity_range    nil})
      (conj {:model_type  model
             :mode        nil
             :wind        nil
             :temperature nil
             :humidity    nil})
      (conj v)))


(defn get-aircon-model
  [access-token device-sym]
  (-> (get-device-profile access-token device-sym)
      :symbol
      aircon-model-symbol))


(defn get-aircon
  [access-token device-sym]
  (-> (if (aircon-model-symbol device-sym)
        (get-device access-token device-sym "aircon")
        (nb-get-aircon access-token device-sym))
      (as-> v
        ;; エアコンのモデルに応じて足りない値を埋める
        (-> (or (aircon-model-symbol device-sym)
                (get-aircon-model access-token device-sym))
            (fix-aircon-model-value v)))))


;; Returns:
;; {:accessible [{:key value ...}]} => アクセス権のあるデバイスの一覧
;; {:accessible nil}              => デバイスはあるが、どれにもアクセス権がない
;; nil                            => デバイスは存在しない
(defn get-devices
  [access-token place]
  (let [all-devices (->> (search-devices access-token place :class nil)
                         (filter (fn [x]
                                   (case (first (:classes x))
                                     "doorlock" true
                                     "light"    true
                                     "aircon"   true
                                     false))))
        accessible  (->> all-devices
                         (filter :accessible))]
    (cond
      (and (not-empty all-devices)
           (empty? accessible))
      {:accessible nil}

      :else
      (some-> accessible
              (as-> ds
                (map
                 (fn [d]
                   (-> d
                       (select-keys [:symbol :name])
                       (clojure.set/rename-keys {:symbol :device_id})
                       (conj
                        (let [type (first (:classes d))]
                          {:device_type type
                           :device_endpoint
                           (str "https://edu-iot.iniad.org/api/v1/"
                                type "s/"
                                (:symbol d))}))))
                 ds))
              (seq)
              (as-> values {:accessible values})))))


;; Returns:
;; {:accessible {:key value ...}} => アクセス権のあるセンサーの値
;; {:accessible nil}              => センサーはあるが、どれにもアクセス権がない
;; nil                            => センサーは存在しない
(defn get-sensors
  [access-token place]
  (let [all-devices (search-devices access-token place)
        accessible  (filter :accessible all-devices)]
    (cond
      (and (not-empty all-devices)
           (empty? accessible))
      {:accessible nil}

      :else
      (some-> (map :id accessible)
              (as-> devices
                (map (partial get-device-status access-token) devices))
              (seq)
              (average)
              (as-> values {:accessible values})))))


(defn update-lockers-acl
  [locker-group]
  (some-> @(h/post
            (str ubiq-authority "/.acl-load/lockers/" locker-group)
            {:timeout     (* 1000 60 2) ; ms
             :oauth-token "clms5l4ndtg0jd7y"
             :headers     util/content-type-json
             :body        "{}"})
          (:status)))


(defn open-locker
  [access-token groupid]
  @(h/post
    (str ubiq-authority "/lockers/" groupid "/unlock")
    {:oauth-token access-token
     :headers     util/content-type-json
     :body        "{}"}))


(defn cards-restructure
  [maps]
  (map (fn [m]
         {:id      (util/as-number (:cardNum m) 0 1000)
          :uid     (:idm m)
          :comment (:comment m)})
       maps))


(defn get-iccards
  [username]
  (let [{status :status body :body} (felica/list-cards {:username username})]
    (if (= status 200)
      body
      (log/error :error body))))


(defn create-iccard
  [username idm comment]
  (some->
   (felica/create-card {:username username :idm idm :comment comment})
   (as-> res
     (cond
       (= 200 (:status res))
       {:status (:status res)
        :body   (some-> (:body res)
                        (as-> cards
                          (filter #(= (:uid %) idm) cards))
                        (seq)
                        (first))}
       :else
       {:status (:status res)
        :body   (some-> (:body res)
                        (:message)
                        (list)
                        (util/error-reply))}))))


(defn update-iccard
  [username card-id comment]
  (some->
   (get-iccards username)
   (as-> cards
     (filter #(= (:id %) card-id) cards))
   (seq)
   (first)
   (as-> card
     (let [{status :status body :body}
           (felica/update-card {:username username
                                :idm      (:uid card)
                                :comment  comment})]
       (if (= status 200)
         body
         (log/error :error body))))))


(defn delete-iccard
  [username card-id]
  (some->
   (get-iccards username)
   (as-> cards
     (filter #(= (:id %) card-id) cards))
   (seq)
   (first)
   (as-> card
     (felica/delete-card {:username username :idm (:uid card)}))
   (:status)
   (as-> sts
     (when (= sts 200)
       {"message" (str "ICCard No." card-id " was deleted")}))))


(defn get-locker-params
  [ctx]
  (or (some-> (get-in ctx [:request ::auth/login :username])
              (as-> user (database/lockers {:user user}))
              (as-> locker (assoc-in ctx [:request ::locker] locker)))
      ctx))


(def locker-params
  {:name  ::locker-interceptor
   :enter get-locker-params})


(defn dooropener-sym->num
  [sym]
  (let [[_ n] (re-matches #"DOOROPENER([0-9]+)" sym)]
    (util/as-number n 0 99)))


(defn get-dooropeners
  [access-token]
  (->> (search-devices access-token nil :symbol true :full true :class "autodoor")
       (map :symbol)
       (map dooropener-sym->num)
       (map #(str "https://edu-iot.iniad.org/api/v1/dooropeners/" %))))


;; Returns:
;; {:accessible {:key value ...}} => アクセス権のある値
;; {:accessible nil}              => アクセス権がない
;; nil                            => 存在しない
(defn get-dooropener
  [access-token num]
  (let [n     (util/as-number num 0 99)
        sym   (format "DOOROPENER%02d" n)
        items (search-devices access-token nil :symbol true :full true :class "autodoor")
        item  (->> items (filter #(= sym (:symbol %))) first)]
    (cond
      (nil? item)              nil
      (not (:accessible item)) {:accessible nil}
      :else                    {:accessible
                                (conj (select-keys item [:name])
                                      {:item_num    n
                                       :device_type "dooropener"}
                                      (get-device-status access-token
                                                         (:id item)
                                                         :class "autodoor"))})))


(defn open-dooropener
  [access-token sym hold]
  @(h/post
    (str ubiq-authority "/autodoors/" sym "/open")
    {:oauth-token access-token
     :headers     util/content-type-json
     :body        (util/json-write-str
                   {:hold_time hold})}))


(defn put-dooropener
  [access-token num hold]
  (let [sym     (format "DOOROPENER%02d" (util/as-number num 0 99))
        hold    (util/as-number hold 0 (* 60 60 24))
        _result (open-dooropener access-token sym hold)]
    (get-dooropener access-token num)))
