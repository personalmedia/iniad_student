(ns student.service
  (:require
   [clj-time.core :as t]
   [clj-time.format :as tf]
   [clojure.string]
   [environ.core :refer [env]]
   [io.pedestal.http :as http]
   [io.pedestal.http.body-params :as body-params]
   [io.pedestal.http.ring-middlewares :as ring-middlewares]
   [io.pedestal.http.secure-headers :as secure-headers]
   [student.aes :as aes]
   [student.auth :as auth]
   [student.cloudlog :refer [cloudlog]]
   [student.database :as database]
   [student.ipcheck :as ipcheck]
   [student.signage :as signage]
   [student.throttle :as throttle]
   [student.timer :as timer]
   [student.ubiq :as ubiq]
   [student.util :as util])
  (:import
   (org.eclipse.jetty.server
    Server
    ServerConnector)))


(defn last-param
  "リクエストパラメータの値が配列の場合は最後の値のみ返す
  パラメータのキーが重複して出現する場合に、このように解釈されるため、正規化する。"
  [request keys]
  (let [value (get-in request keys)]
    (cond
      (string? value)     value
      (sequential? value) (last value)
      :else               value)))


(defn get-param
  [request key]
  (cond
    (:json-params request)      (last-param request [:json-params key])
    (:form-params request)      (last-param request [:form-params key])
    (:multipart-params request) (last-param request [:multipart-params (name key)])
    :else                       nil))


(defn get-other-keys
  [request ks]
  (cond
    (:json-params request)      (keys (apply dissoc (-> request :json-params) ks))
    (:form-params request)      (keys (apply dissoc (-> request :form-params) ks))
    (:multipart-params request) (keys (apply dissoc (-> request :multipart-params)
                                             (map name ks)))
    :else                       nil))


(defn notifications-page
  [request]
  (when-let [token (get-in request [::auth/login :access_token])]
    (let [secret  (env :edu-token-secret "e7cd9cd9f09f947be7cd9cd9f09f947b")
          encoded (aes/enc token secret)]
      {:status 200
       :body   {:url   "wss://portal.iniad.org:8201/"
                :realm encoded
                :topic []}})))


(defn one-device-page
  [request device-type getter]
  (if-let [room-num (::util/room-num request)]
    (if-let [number (get-in request [:path-params :number])]
      (some-> (get-in request [::auth/login :access_token])
              (ubiq/get-devices-in-room
               room-num
               :class device-type
               :item-no number
               :f (fn [x]
                    (->
                     (getter (get-in request [::auth/login :access_token])
                             (:ucode x))
                     ;; ユビキタスサーバの返す値に追加情報を加える
                     (assoc :room_num (str (:room_no x))
                            :item_num (:item_no x)
                            :device_type device-type))))
              (as-> results
                (if-let [sensors (:accessible results)]
                  (some-> sensors
                          (seq)
                          first
                          (as-> vals
                            {:status 200
                             :body   vals}))
                  util/devices-not-accessible-error-reply)))
      util/room-num-error-reply)
    util/room-num-error-reply))


(defn aircon-page
  [request]
  (one-device-page request "aircon" ubiq/get-aircon))


(defn light-page
  [request]
  (one-device-page request "light" ubiq/get-light))


(defn doorlock-page
  [request]
  (one-device-page request "doorlock" ubiq/get-doorlock))


(defn change-aircon-page
  [request]
  (let [json?  (:json-params request)
        on     (get-param request :on)
        mode   (get-param request :mode)
        wind   (get-param request :wind)
        temp   (when-let [x (get-param request :temperature)]
                 (if json?
                   (if (number? x) x -1)
                   (util/string->int x :default -1)))
        humi   (when-let [x (get-param request :humidity)]
                 (if json?
                   (if (number? x) x -1)
                   (util/string->int x :default -1)))
        ;; 指定可能以外のパラメータがある場合
        more?  (get-other-keys request [:on :mode :wind :temperature :humidity])
        range? (fn [v [b e]] (and b e v (<= b v e)))
        on-v   (if json? [true false] ["true" "false"])
        ins    (fn [m k v] (if v (assoc m k v) m))
        ins-b  (fn [m k v] (if (not (nil? v)) (assoc m k (= "true" (str v))) m))
        f      (fn [access-token x]
                 ;; デバイスIDから取り得る値の範囲を取得
                 (let [possibility (-> (ubiq/get-aircon-model access-token x)
                                       (ubiq/fix-aircon-model-value {}))]
                   (if (some #(= % mode) (conj (:modes possibility) nil))
                     (if (some #(= % wind) (conj (:winds possibility) nil))
                       (if (or (nil? temp)
                               (range? temp (:temperature_range possibility)))
                         (if (or (nil? humi)
                                 (range? humi (:humidity_range possibility)))
                           ;; 指定値が可能な範囲に収まっている時のみ実行
                           (conj possibility
                                 (ubiq/set-aircon access-token x
                                                  (-> {}
                                                      (ins-b :on on)
                                                      (ins :mode mode)
                                                      (ins :wind wind)
                                                      (ins :temperature temp)
                                                      (ins :humidity humi))))
                           {:error (util/aircon-set-state-range-error-reply
                                    "humidity" (:humidity_range possibility))})
                         {:error (util/aircon-set-state-range-error-reply
                                  "temperature" (:temperature_range possibility))})
                       {:error (util/aircon-set-state-value-error-reply
                                "wind" (:winds possibility))})
                     {:error (util/aircon-set-state-value-error-reply
                              "mode" (:modes possibility))})))]
    (if (not more?)
      (if (some #(= % on) (conj on-v nil))
        (let [result (one-device-page request "aircon" #(f %1 %2))]
          (if-let [err (-> result :body :error)]
            err
            result))
        (util/aircon-set-state-value-error-reply "on" on-v))
      util/aircon-set-state-param-error-reply)))


(defn string->bool
  [s]
  (case s
    "true"  [true true]
    "false" [false true]
    [nil false]))


(defn change-doorlock-page
  [request]
  (let [json?           (:json-params request)
        value           (get-param request :locked)
        ;; 正当な値か
        [value' valid?] (if json?
                          [value (boolean? value)]
                          (string->bool value))
        ;; 指定可能以外のパラメータがある場合
        more?           (get-other-keys request [:locked])
        f               (fn [access-token x]
                          (ubiq/set-doorlock access-token x value'))]
    (cond
      (not valid?) (util/device-set-state-param-value-error-reply "locked" "true or false")
      more?        (util/device-set-state-param-error-reply "locked")
      :else        (one-device-page request "doorlock" f))))


(defn change-light-page
  [request]
  (let [value  (get-param request :mode)
        ;; 指定可能以外のパラメータがある場合
        more?  (get-other-keys request [:mode])
        ;; 型のチェック
        valid? (string? value)
        f      (fn [access-token x]
                 (ubiq/set-light access-token x value))]
    (cond
      more?        (util/device-set-state-param-error-reply "mode")
      (not valid?) (util/device-set-state-param-error-reply "mode")
      :else        (one-device-page request "light" f))))


(defn devices-page
  [request device-type]
  (if-let [room-num (::util/room-num request)]
    (some-> (get-in request [::auth/login :access_token])
            (ubiq/get-devices-in-room
             room-num
             :class device-type
             :f (fn [x]
                  (str "https://edu-iot.iniad.org/api/v1/"
                       device-type "s/"
                       (:room_no x) "/"
                       (:item_no x))))
            (as-> results
              (if-let [sensors (:accessible results)]
                (some-> sensors
                        (seq)
                        (as-> vals
                          {:status 200
                           :body   vals}))
                util/devices-not-accessible-error-reply)))
    util/room-num-error-reply))


(defn aircons-page
  [request]
  (devices-page request "aircon"))


(defn doorlocks-page
  [request]
  (devices-page request "doorlock"))


(defn lights-page
  [request]
  (devices-page request "light"))


(defn get-room-devices
  "デバイス番号マッピングの元帳を生成する"
  [request]
  (some-> (get-in request [::auth/login :access_token])
          ubiq/get-room-devices-to-string
          (as-> data
            {:status  200
             :headers {"Content-Type" "text/csv"}
             :body    data})))


(defn device-renumber-page
  [request]
  (let [access  (get-in request [::auth/login :access_token])
        room-no (some-> (get-in request [:path-params :room_num])
                        (util/as-number 1000 6001))
        item-no (some-> (get-in request [:path-params :number])
                        (util/as-number 0 1000))
        to-no   (some-> (get-in request [:params "to"])
                        (util/as-number 0 1000))
        class   "light"]
    (if (every? identity [access room-no item-no to-no])
      (when-let [body (ubiq/renumber-device class room-no item-no to-no)]
        (-> body
            first
            (as-> x
              {:status 200
               :body   {:url     (str "https://edu-iot.iniad.org/api/v1/"
                                      class "s/"
                                      (:room_no x) "/"
                                      (:item_no x))
                        :from_no item-no
                        :to_no   to-no}})))
      {:status 400
       :body   (util/error-reply ["Invalid paramemers"])})))


(defn accessibility-page
  [request]
  (if-let [room-num (some-> (get-in request [:path-params :room_num])
                            (util/as-number 1000 10000))]
    (when-let [access-token (get-in request [::auth/login :access_token])]
      (let [proxy-user (or (last-param request [:params :proxy_user])
                           (get-in request [::auth/login :username]))
            proxy-time (or (last-param request [:params :proxy_time])
                           (-> (tf/unparse
                                (tf/formatter-local "yyyy-MM-dd HHmm")
                                (t/to-time-zone
                                 (t/now)
                                 (t/time-zone-for-offset 9)))
                               (clojure.string/replace " " "T")))]
        {:status 200
         :body   (ubiq/get-accessibility-in-room
                  access-token room-num
                  :proxy-as {:user proxy-user
                             :time proxy-time})}))
    nil))


(defn sensors-page
  [request]
  (if-let [room-num (::util/room-num request)]
    (some-> (get-in request [::auth/login :access_token])
            (ubiq/get-sensors room-num)
            (as-> results
              (if-let [sensors (:accessible results)]
                (some-> sensors
                        (select-keys (into [] (::util/sensor-type-* request)))
                        (as-> sensors
                          (map (fn [x]
                                 {:room_num
                                  (get-in request [:path-params :room_num])
                                  :sensor_type (name x)
                                  :value       (x sensors)})
                               (keys sensors)))
                        (seq)
                        (as-> vals
                          {:status 200
                           :body   vals}))
                util/sensors-not-accessible-error-reply)))
    util/room-num-error-reply))


(defn sensor-page
  [request]
  (if-let [room-num (::util/room-num request)]
    (some-> (get-in request [::auth/login :access_token])
            (ubiq/get-sensors room-num)
            (as-> results
              (if-let [sensors (:accessible results)]
                (some-> sensors
                        (select-keys [(::util/sensor-type-1 request)])
                        (seq)
                        (first)
                        (as-> kv
                          (apply
                           (fn [k v]
                             {:room_num
                              (get-in request [:path-params :room_num])
                              :sensor_type (name k)
                              :value       v}) kv))
                        (as-> vals
                          {:status 200
                           :body   vals}))
                util/sensors-not-accessible-error-reply)))
    util/room-num-error-reply))


(defn locker-name->locker
  [name]
  (let [floor (util/string->int (subs name 0 1))]
    {:name name :floor floor}))


(defn locker-page
  [request]
  (some-> (get-in request [::auth/login :username])
          (as-> user (database/lockers {:user user}))
          (:adr)
          (locker-name->locker)
          (as-> vals
            {:status 200
             :body   vals})))


(defn locker-open-page
  [request]
  (some-> (get-in request [::auth/login :username])
          (as-> user (database/lockers {:user user}))
          (as-> locker
            (when (:adr locker)
              (some->
               (ubiq/open-locker
                (get-in request [::auth/login :access_token])
                (:groupid locker))
               (:status)
               (as-> status
                 (if (= 200 status)
                   {:status 200
                    :body   (locker-name->locker (:adr locker))}
                   util/locker-open-error-reply)))))))


(defn get-status-locker-status
  [locker access-token]
  (when-let [status (ubiq/get-device-status access-token (:groupid locker) :class "locker")]
    (conj (locker-name->locker (:adr locker))
          status)))


(defn status-locker-page
  [request type]
  (some-> (get-in request [::auth/login :username])
          (as-> user (database/lockers {:user user :type type}))
          (get-status-locker-status (get-in request [::auth/login :access_token]))
          (as-> vals
            {:status 200
             :body   vals})))


(defn pc-locker-page
  [request]
  (status-locker-page request 2))


(defn sm-locker-page
  [request]
  (status-locker-page request 3))


(defn status-locker-open-page
  [request type]
  (some-> (get-in request [::auth/login :username])
          (as-> user (database/lockers {:user user :type type}))
          (as-> locker
            (some-> (ubiq/open-locker (get-in request [::auth/login :access_token])
                                      (:groupid locker))
                    (:status)
                    (as-> status
                      (cond
                        (= 200 status)
                        (if-let [status (get-status-locker-status locker
                                                                  (get-in request [::auth/login :access_token]))]
                          {:status 200
                           ;; 解錠直後にステータスを取っても locked: true のままになってしまうため、
                           ;; 応答は false で上書きしておく。(解錠に成功したのだから false でよいはず。)
                           ;; 数秒後であれば、正しくステータスが取れる、という事のようだ。
                           :body   (assoc status :locked false)}
                          util/locker-open-error-reply)

                        :else util/locker-open-error-reply))))))


(defn pc-locker-open-page
  [request]
  (status-locker-open-page request 2))


(defn sm-locker-open-page
  [request]
  (status-locker-open-page request 3))


;; ロッカーグループがあれば更新登録する。
;; サイドエフェクトがメインで ctx をそのまま返す。
(defn locker-group-update-reservation!
  [ctx request]
  (some-> (::ubiq/locker request)
          (as-> locker
            (when (:adr locker)
              (:groupid locker)))
          (timer/add-item))
  ctx)


(defn iccards-page
  [request]
  (some-> (get-in request [::auth/login :username])
          (ubiq/get-iccards)
          (as-> vals
            {:status 200
             :body   vals})))


(defn iccards-post-page
  [request]
  (if-let [idm (::util/card-idm request)]
    (some-> (get-in request [::auth/login :username])
            (ubiq/create-iccard idm
                                (get-in request [:params "comment"]))
            (locker-group-update-reservation! request)
            ;; create-iccard の値はそのままレスポンスになっている
            )
    util/card-idm-error-reply))


(defn iccard-page
  [request]
  (if-let [card-id (::util/card-id request)]
    (some-> (get-in request [::auth/login :username])
            (ubiq/get-iccards)
            (as-> cards
              (filter #(= (:id %) card-id) cards))
            (seq)
            (first)
            (as-> vals
              {:status 200
               :body   vals}))
    util/card-id-error-reply))


(defn iccard-put-page
  [request]
  (if-let [card-id (::util/card-id request)]
    (some-> (get-in request [::auth/login :username])
            (ubiq/update-iccard card-id
                                (str (get-param request :comment)))
            (as-> vals
              {:status 200
               :body   vals}))
    util/card-id-error-reply))


(defn iccard-patch-page
  [request]
  (iccard-put-page request))


(defn iccard-delete-page
  [request]
  (if-let [card-id (::util/card-id request)]
    (some-> (get-in request [::auth/login :username])
            (ubiq/delete-iccard card-id)
            (locker-group-update-reservation! request)
            (as-> vals
              {:status 200
               :body   vals}))
    util/card-id-error-reply))


;; Defines "/" and "/about" routes with their associated :get handlers.
;; The interceptors defined after the verb map (e.g., {:get home-page}
;; apply to / and its children (/about).
(def common
  [(secure-headers/secure-headers
    {:content-security-policy-settings {:default-src "'self'"
                                        :object-src  "'none'"}})
   ;; レスポンスを JSON に変換します
   http/json-body

   util/app-error-handler
   util/json-throwing-interceptor
   (body-params/body-params)
   (ring-middlewares/multipart-params)
   cloudlog
   ipcheck/ip-white-list
   auth/login
   throttle/throttle-by-username
   util/params])


;; カード更新するAPIの場合、ロッカーグループが必要
(def locker
  (conj common
        ubiq/locker-params))


(defn admin-table-get
  [request]
  (let [table (-> request :path-params :table)
        reply (signage/get-priv table)]
    {:status 200
     :body   reply}))


(defn list-params
  [request key]
  (or (-> request :json-params key)
      (some-> request :form-params key
              (clojure.string/split #"[, ]+"))))


(defn admin-table-put
  [request]
  (let [admin-roles (signage/get-priv "admin")
        user-roles  (-> request auth/requester-roles)]
    (if (not (signage/has-role user-roles admin-roles))
      util/signage-not-accessible-error-reply

      (let [table  (-> request :path-params :table)
            groups (-> request (list-params :groups))
            reply  (signage/put-priv table groups)]
        {:status (:status reply)
         :body   (:body reply)}))))


(defn single-param
  [request key]
  (or (-> request :json-params key)
      (some-> request :form-params key)))


(defn signage-list
  [_request]
  (let [reply (signage/list-signage)]
    {:status 200
     :body   reply}))


(defn signage-acc-get
  [request]
  (let [signage (-> request :path-params :signage)
        reply   (signage/get-signage-acc signage)]
    {:status 200
     :body   reply}))


(defn signage-acc-put
  [request]
  (let [admin-roles (signage/get-priv "signage")
        user-roles  (-> request auth/requester-roles)]
    (if (not (signage/has-role user-roles admin-roles))
      util/signage-not-accessible-error-reply

      (let [signage (-> request :path-params :signage)
            read    (-> request (list-params :read)  signage/printable-strings)
            write   (-> request (list-params :write) signage/printable-strings)
            reply   (signage/put-signage-acc signage read write)]
        {:status 204
         :body   reply}))))


(defn signage-item-get
  [request]
  (let [signage (-> request :path-params :signage)
        item    (-> request :path-params :item)]
    (case item
      "role" (signage-acc-get request)

      (let [user-roles (-> request auth/requester-roles)]
        (cond
          (not (signage/has-rights :acc_read signage user-roles))
          util/signage-not-accessible-error-reply

          :else
          (let [reply (signage/get-signage-item signage item)]
            (case (long (or (:status reply) 500))
              200 reply
              406 {:status 404
                   :body   ""}
              {:status 400
               :body   ""})))))))


(defn signage-item-put
  [request]
  (let [signage (-> request :path-params :signage)
        item    (-> request :path-params :item)]
    (case item
      "role" (signage-acc-put request)

      (let [username   (-> request ::auth/login :username)
            user-roles (-> request auth/requester-roles)
            title      (-> request (single-param :title) signage/printable-string)
            playtime   (-> request (single-param :playtime))]
        (cond
          (not (signage/has-rights :acc_write signage user-roles))
          util/signage-not-accessible-error-reply

          :else
          (let [reply (signage/put-signage-item signage item username
                                                {:title    title
                                                 :playtime playtime})]
            {:status 200
             :body   reply}))))))


(defn signage-item-delete
  [request]
  (let [signage    (-> request :path-params :signage)
        item       (-> request :path-params :item)
        user-roles (-> request auth/requester-roles)]
    (cond
      (not (signage/has-rights :acc_write signage user-roles))
      util/signage-not-accessible-error-reply

      :else
      (let [reply (signage/delete-signage-item signage item)]
        {:status 204
         :body   reply}))))


(defn signage-image-get
  [request]
  (let [signage    (-> request :path-params :signage)
        item       (-> request :path-params :item)
        user-roles (-> request auth/requester-roles)]
    (if (not (signage/has-rights :acc_read signage user-roles))
      util/signage-not-accessible-error-reply

      (let [reply (signage/get-signage-image signage item)]
        reply))))


(defn signage-image-put
  [request]
  (let [signage    (-> request :path-params :signage)
        item       (-> request :path-params :item)
        username   (-> request ::auth/login :username)
        user-roles (-> request auth/requester-roles)
        file       (-> request :multipart-params (get "file"))]
    (comment
      ;; ファイルは以下のように渡されてくる
      :multipart-params {"file" {:filename     "Fast.jpg",
                                 :content-type "image/jpeg",
                                 :tempfile     "#object[java.io.File]",
                                 :size         44653}})
    (cond
      (not (signage/has-rights :acc_write signage user-roles))
      util/signage-not-accessible-error-reply

      (not (<= 0 (or (:size file) 0) (* 1024 1024 1024)))
      {:status 422
       :body   ""}

      :else
      (let [reply (signage/put-signage-image signage item username (:tempfile file))]
        reply))))


(defn signage-get
  [request]
  (let [signage (-> request :path-params :signage)
        reply   (signage/list-signage-item signage)]
    {:status 200
     :body   reply}))


(defn signage-put
  [request]
  (let [admin-roles (signage/get-priv "signage")
        user-roles  (-> request auth/requester-roles)]
    (if (not (signage/has-role user-roles admin-roles))
      util/signage-not-accessible-error-reply

      (let [signage (-> request :path-params :signage)
            reply   (signage/put-signage signage admin-roles)]
        {:status (:status reply)
         :body   (:body reply)}))))


(defn signage-delete
  [request]
  (let [admin-roles (signage/get-priv "signage")
        user-roles  (-> request auth/requester-roles)]
    (if (not (signage/has-role user-roles admin-roles))
      util/signage-not-accessible-error-reply

      (let [signage (-> request :path-params :signage)
            reply   (signage/delete-signage signage)]
        {:status (:status reply)
         :body   (:body reply)}))))


(defn dooropeners-page
  [request]
  (let [token  (get-in request [::auth/login :access_token])
        result (ubiq/get-dooropeners token)]
    {:status 200
     :body   result}))


(defn dooropener-page
  [request]
  (let [token  (get-in request [::auth/login :access_token])
        number (-> request :path-params :number)
        result (ubiq/get-dooropener token number)]
    (cond
      (nil? result)              nil
      (not (:accessible result)) util/devices-not-accessible-error-reply
      :else                      {:status 200 :body (:accessible result)})))


(defn dooropener-edit-page
  [request]
  (let [token  (get-in request [::auth/login :access_token])
        number (-> request :path-params :number)
        hold   (-> request (single-param :time))
        result (ubiq/put-dooropener token number hold)]
    (cond
      (nil? result)              nil
      (not (:accessible result)) util/devices-not-accessible-error-reply
      :else                      {:status 200 :body (:accessible result)})))


(defn about-page
  [request]
  {:status 200
   :body   (:headers request)})


(defn health-check-page
  "GCPロードバランサのヘルスチェックに200応答する"
  [_request]
  {:status 200
   :body   ""})


(defn ubiq-proxy-any
  "ユビキタスサーバ Proxy"
  [request]
  (let [path  (get-in request [:path-params :path])
        meth  (get-in request [:request-method])
        query (get-in request [:query-params])
        json  (get-in request [:json-params])
        token (get-in request [::auth/login :access_token])]
    (ubiq/send-proxy-request token path meth query json)))


(defn ubiq-proxy-get
  [request]
  (ubiq-proxy-any request))


(defn ubiq-proxy-post
  [request]
  (ubiq-proxy-any request))


(defn ubiq-proxy-put
  [request]
  (ubiq-proxy-any request))


;; Tabular routes
(def routes
  #{["/api/v1/sensors/:room_num"              :get (conj common `sensors-page)]
    ["/api/v1/sensors/:room_num/:sensor_type" :get (conj common `sensor-page)]

    ["/api/v1/aircons/:room_num"   :get (conj common `aircons-page)]
    ["/api/v1/doorlocks/:room_num" :get (conj common `doorlocks-page)]
    ["/api/v1/lights/:room_num"    :get (conj common `lights-page)]

    ["/api/v1/aircons/:room_num/:number"   :get (conj common `aircon-page)]
    ["/api/v1/doorlocks/:room_num/:number" :get (conj common `doorlock-page)]
    ["/api/v1/lights/:room_num/:number"    :get (conj common `light-page)]

    ["/api/v1/lights/:room_num/:number/renumber" :put
     (conj common `device-renumber-page)]

    ["/api/v1/aircons/:room_num/:number"   :put (conj common `change-aircon-page)]
    ["/api/v1/doorlocks/:room_num/:number" :put (conj common `change-doorlock-page)]
    ["/api/v1/lights/:room_num/:number"    :put (conj common `change-light-page)]

    ;; 多人数ロッカー
    ["/api/v1/locker"      :get  (conj common `locker-page)]
    ["/api/v1/locker/open" :post (conj common `locker-open-page)]

    ;; PCロッカー、小物ロッカー
    ["/api/v1/pclocker"         :get  (conj common `pc-locker-page)]
    ["/api/v1/pclocker/open"    :post  (conj common `pc-locker-open-page)]
    ["/api/v1/smalllocker"      :get  (conj common `sm-locker-page)]
    ["/api/v1/smalllocker/open" :post  (conj common `sm-locker-open-page)]

    ;; 自動ドア
    ["/api/v1/dooropeners"         :get  (conj common `dooropeners-page)]
    ["/api/v1/dooropeners/:number" :get  (conj common `dooropener-page)]
    ["/api/v1/dooropeners/:number" :post (conj common `dooropener-edit-page)]

    ;; ICカード
    ["/api/v1/iccards"          :get    (conj common `iccards-page)]
    ["/api/v1/iccards"          :post   (conj locker `iccards-post-page)]
    ["/api/v1/iccards/:card-id" :get    (conj common `iccard-page)]
    ["/api/v1/iccards/:card-id" :put    (conj common `iccard-put-page)]
    ["/api/v1/iccards/:card-id" :patch  (conj common `iccard-patch-page)]
    ["/api/v1/iccards/:card-id" :delete (conj locker `iccard-delete-page)]

    ;; サイネージ
    ["/api/v1/signages" :get (conj common `signage-list)]
    ["/api/v1/signages/:signage" :get    (conj common `signage-get)]
    ["/api/v1/signages/:signage" :put    (conj common `signage-put)]
    ["/api/v1/signages/:signage" :delete (conj common `signage-delete)]
    ["/api/v1/signages/:signage/:item/data" :get  (conj common `signage-image-get)]
    ["/api/v1/signages/:signage/:item/data" :post (conj common `signage-image-put)]
    ["/api/v1/signages/:signage/:item" :get    (conj common `signage-item-get)]
    ["/api/v1/signages/:signage/:item" :put    (conj common `signage-item-put)]
    ["/api/v1/signages/:signage/:item" :delete (conj common `signage-item-delete)]
    ["/api/v1/admin/:table" :get (conj common `admin-table-get)]
    ["/api/v1/admin/:table" :put (conj common `admin-table-put)]

    ;; ユビキタスサーバ Proxy
    ["/api/v1/ubiq/*path" :get (conj common `ubiq-proxy-get)]
    ["/api/v1/ubiq/*path" :post (conj common `ubiq-proxy-post)]
    ["/api/v1/ubiq/*path" :put (conj common `ubiq-proxy-put)]

    ;; その他
    ["/api/v1/accessibility/:room_num" :get (conj common `accessibility-page)]

    ["/api/v1/notifications" :get (conj locker `notifications-page)]

    ["/api/v1/_devicemaps" :get (conj common `get-room-devices)]

    ["/_ah/health" :get `health-check-page]

    ;; for Debug.
    ;; ["/api/v1/about" :post (conj common `about-page)]
    })


(defn container-configurator
  [server]
  ;; GCPロードバランサをフロントに立てることを前提に、キープアライブを長めにとる
  (->> (.getConnectors ^Server server)
       (map #(.setIdleTimeout ^ServerConnector % (* 1000 60 60))))
  server)


;; Consumed by student.server/create-server
;; See http/default-interceptors for additional options you can configure
(def service
  {:env          :prod
   ;; You can bring your own non-default interceptors. Make
   ;; sure you include routing and set it up right for
   ;; dev-mode. If you do, many other keys for configuring
   ;; default interceptors will be ignored.
   ;; ::http/interceptors []
   ::http/routes routes

   ::http/not-found-interceptor util/not-found-error

   ;; Uncomment next line to enable CORS support, add
   ;; string(s) specifying scheme, host and port for
   ;; allowed source(s):
   ;;
   ;; "http://localhost:8080"
   ;;
   ;; あらゆる Origin からのリソースアクセスを許可する
   ::http/allowed-origins {:creds           true
                           :max-age         432000 ; 5 days
                           :allowed-origins (constantly true)}

   ;; Root for resource interceptor that is available by default.
   ::http/resource-path "/public"

   ;; Either :jetty, :immutant or :tomcat (see comments in project.clj)
   ::http/type              :jetty
   ::http/host
   (or (env :student-host) "localhost")
   ::http/port
   (try (bigdec (env :student-port)) (catch Exception _ 8080))
   ;; Options to pass to the container (Jetty)
   ::http/container-options {:configurator container-configurator
                             :h2c?         false
                             :h2?          false
                             ;; :keystore "server.pkcs12"
                             ;; :key-password "CHANGE ME!"
                             ;; :ssl-port 8443
                             :ssl?         false}})
