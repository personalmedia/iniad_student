(ns student.timer
  (:require
   [clj-time.core :as t]
   [clojure.core.async :as a :refer [<!]]
   [clojure.set :as set]
   [io.pedestal.log :as log]
   [student.ubiq :as ubiq]))


(def interval 3)

(defonce time-table (ref {}))


;; minute 分単位の時刻を返す
(defn quantum-time
  [now minute]
  (let [y (t/year now)
        m (t/month now)
        d (t/day now)
        h (t/hour now)
        n (t/minute now)
        q (t/date-time y m d h n)
        r (- minute (rem n minute))]
    (t/plus q (t/minutes r))))


(defn add-item
  [item]
  (let [tgroup (-> (t/now)
                   (t/plus (t/seconds 120))
                   (quantum-time interval))]
    (dosync
     (alter time-table
            #(update %1 tgroup
                     (fn [v]
                       (if v
                         (conj v %2)
                         (set [%2]))))
            item))))


(defn pop-item
  []
  (let [now (t/now)]
    (dosync
     (let [ks (->> @time-table
                   (keys)
                   (filter #(t/before? % now))
                   (seq))]
       (when ks
         (let [kss   (set ks)
               items (reduce-kv
                      (fn [m k v] (if (kss k) (set/union m v) m))
                      #{} @time-table)]
           (alter time-table #(apply dissoc % ks))
           items))))))


(defn spy
  [msg val]
  (log/info :timer (str msg ": " (seq val)))
  val)


(defn periodic-work
  []
  (log/info :timer "periodic start")
  (some->> (pop-item)
           (spy "items")
           (map (fn [group]
                  {:group  group
                   :status (ubiq/update-lockers-acl group)}))
           (spy "status")
           (filter #(not= 200 (:status %)))
           (seq)
           (map :group)
           (spy "not 200 groups")
           ;; 200応答でなかったグループは、後で再度更新をかける。
           (map add-item)))


(defonce started (atom false))


(defn start-time-worker
  []
  (when-not @started
    (reset! started true)
    (a/go-loop []
      (<! (a/timeout (* 1000 60)))
      (try (periodic-work)
           (catch Exception _ nil))
      (recur))))
