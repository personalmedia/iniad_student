(ns student.auth
  (:require
   [clojure.core.cache.wrapped :as cache]
   [io.pedestal.interceptor.chain :as chain]
   [io.pedestal.log :as log]
   [jerks-whistling-tunes.core :as jwt]
   [jerks-whistling-tunes.sign :as jwt-sign]
   [ring.util.codec :as codec]
   [student.auth-config :as auth-config]
   [student.keycloak :as keycloak]
   [student.util :as util]))


(def ^:private openam auth-config/oidc)


(defn requester-roles
  "リクエストコンテキストのユーザ情報からロールを返す"
  [request]
  (-> request ::login :userinfo/roles))


(defn get-access-token
  [ctx username password]
  (let [cred (keycloak/get-access-token
              ctx
              {:username  username
               :password  password
               :client_id (:client_id openam)
               :scope     (:scope openam)})]
    (if (get-in cred [:body :access_token])
      (let [body (:body cred)]
        (conj body
              {:username (or
                          ;; 認証サーバでのユーザ識別子
                          (:userinfo/uid body)
                          ;; もしくはユーザが認証に使用したID
                          username)}))
      (log/warn :username username
                :login-failed (:body cred)
                :status (:status cred)))))


(def ^:private login-cache (cache/fifo-cache-factory {} :threshold 1024))


(defn get-login
  [username password]
  (when-let [ckey (try (jwt/encode {:username username} (jwt-sign/hs256 password))
                       (catch Exception _))]
    (let [ctx (cache/lookup login-cache ckey (atom {}))]
      (if-let [tokens (get-access-token ctx username password)]
        (do (cache/miss login-cache ckey ctx)
            tokens)
        (do (cache/evict login-cache ckey)
            nil)))))


(def auth-fail-header
  {"www-authenticate" "Basic realm=\"INIAD API\""})


(defn terminate-no-login
  [ctx]
  (-> ctx
      (assoc :response
             {:status  401
              :headers auth-fail-header
              :body    (util/error-reply
                        ["Authorization header required"])})
      (chain/terminate)))


(defn terminate-login-failed
  [ctx]
  (-> ctx
      (assoc :response
             {:status  401
              :headers auth-fail-header
              :body    (util/error-reply
                        ["'username' or 'password' is invalid"])})
      (chain/terminate)))


(defn basic-auth-header
  [header-content]
  (some-> (not-empty header-content)
          (as-> s (re-find #"^Basic\s+(.*)$" s))
          (last)
          (codec/base64-decode)
          (as-> b (apply str (map char b)))
          (as-> s (re-find #"^([^:]+):(.*)$" s))
          (as-> v {:username (get v 1)
                   :password (get v 2)})))


(defn enter-login
  [ctx]
  (if-let [userinfo (-> (get-in ctx [:request :headers "authorization"])
                        (basic-auth-header))]
    (if-let [login (get-login (:username userinfo) (:password userinfo))]
      (update ctx :request assoc ::login login)
      (terminate-login-failed ctx))
    (terminate-no-login ctx)))


(def login
  {:name  ::login-interceptor
   :enter enter-login})
