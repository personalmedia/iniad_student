(ns student.ipcheck
  (:require
   [clojure.string :as str]
   [io.pedestal.interceptor.chain :as chain]
   [io.pedestal.log :as log]
   [ring.util.codec :as codec]
   [student.ubiq :as ubiq]
   [student.util :as util]))


;; Debug Note:
;; localhost:8080 で curl でテストするときは、
;; -H 'x-forwarded-for: 118.238.221.73, 35.190.18.235'
;; を付けてプロキシを通過したふりをするとよい。

#_
(def valid-ip-list
  (set/union
   #{"133.79.207.10"
     "104.198.92.238"
     "118.238.221.73"}
   (->> (range 16 43) (map #(str "133.79.192." %)) (set))))


;; https://cloud.google.com/compute/docs/load-balancing/http/?hl=ja
;;
;; ターゲット プロキシ
;;
;; ターゲット プロキシはクライアントからの HTTP（S）接続を終了し、1 つ
;; 以上のグローバル転送ルールから参照され、URL マップへの受信リクエス
;; トをルーティングします。
;;
;; プロキシは、次のように HTTP リクエスト ヘッダー / レスポンス ヘッダーを設定します。
;;
;; Via: 1.1 google（リクエストとレスポンス）
;;
;; X-Forwarded-Proto: [http | https]（リクエストのみ）
;;
;; X-Forwarded-For: <unverified IP(s)>, <immediate client IP>, <global
;; forwarding rule external IP>, <proxies running in GCP> （リクエスト
;; のみ）リクエストが経由する中間経路で追加される IP アドレスのカンマ
;; 区切りのリスト。データを X-forward-For ヘッダーに追加するプロキシを
;; GCP 内で実行している場合、ソフトウェアでそれらのプロキシの存在と数
;; を考慮する必要があります。<immediate client IP> と <global
;; forwarding rule external IP> の項目のみがロードバランサによって提供
;; されます。リスト内のその他のすべての項目は検証されずに通過します。
;; <immediate client IP> の項目は、ロードバランサに直接属するクライア
;; ントです。<global forwarding rule external IP> の項目は、ロードバラ
;; ンサの転送ルールの外部 IP アドレスです。それよりも項目が多い場合、
;; リスト内の最初の項目が元のクライアントのアドレスになります。
;; <immediate client IP> より前の項目は、ロードバランサにリクエストを
;; 転送する他のプロキシを表します。
;;
;; X-Cloud-Trace-Context: <trace-id>/<span-id>;<trace-options> （リク
;; エストのみ）Stackdriver Trace のパラメータ。
;;

(defn is-connect-from-acceptable-ip-address
  [ctx]
  (let [ip-from (some-> (get-in ctx [:request :headers "x-forwarded-for"])
                        (str/split #",\s*")
                        (as-> it
                          (when (= 2 (count it)) it))
                        (first))]
    (ubiq/ip-is-allow? ip-from)))


(defn terminate
  [ctx]
  (-> ctx
      (assoc :response
             {:status 503
              :body (util/error-reply
                     ["Service available only on INIAD LAN"])})
      (chain/terminate)))


(defn log-who-is
  [ctx]
  (try
    (log/info :who-deny
              (some-> (get-in ctx [:request :headers "authorization"])
                      (str/split #"\s+")
                      second
                      codec/base64-decode
                      (as-> b (apply str (map char b)))
                      (str/split #":")
                      first))
    (catch Exception _ nil)))


(def ip-white-list
  {:name ::ip-white-list-interceptor
   :enter
   (fn [ctx]
     (if (is-connect-from-acceptable-ip-address ctx)
       ctx
       (do
         (log-who-is ctx)
         (terminate ctx))))})
