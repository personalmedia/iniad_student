(ns student.util
  (:require
   [clj-time.coerce :as tc]
   [clj-time.core :as t]
   [clojure.data.json :as json]
   [clojure.string :as str]
   [io.pedestal.http :as http]
   [io.pedestal.interceptor :as interceptor]
   [io.pedestal.interceptor.chain :as chain]
   [io.pedestal.interceptor.error :as error]))


(def ^:const content-type-json
  "HTTPリクエストヘッダの Content-Type に JSON を指定する"
  {"content-type" "application/json; charset=utf-8"})


(defn json-read-str
  [str]
  (try (json/read-str str :key-fn keyword)
       (catch Exception _ nil)))


(defn json-write-str
  [json]
  (json/write-str json))


(defn string->int
  [s & {:keys [default]}]
  (or (and s (parse-long s)) default))


(defn as-number
  [s min max]
  (when-let [n (try (parse-long s)
                    (catch Exception _))]
    (when (< min n max)
      n)))


(defn epoch-now
  "UNIX Epoch 秒"
  []
  (-> (t/now) tc/to-epoch))


(defn error-reply
  [descs]
  {:status      "error"
   :description (some->> (filter identity descs)
                         (str/join ",  "))})


(def aircon-set-state-param-error-reply
  {:status 400
   :body   (error-reply [(str "The parameter must be '"
                              (clojure.string/join
                               "', '" ["on" "mode" "wind" "temperature" "humidity"])
                              "'")])})


(defn aircon-set-state-value-error-reply
  [param valid-params]
  (if (seq valid-params)
    {:status 400
     :body   (error-reply [(str "'" param "' must be '"
                                (clojure.string/join "', '" valid-params)
                                "'")])}
    {:status 400
     :body   (error-reply [(str "'" param "' can not be specified")])}))


(defn aircon-set-state-range-error-reply
  [param [from to]]
  (if (and from to)
    {:status 400
     :body   (error-reply [(str "'" param "' must be from " from " to " to)])}
    {:status 400
     :body   (error-reply [(str "'" param "' can not be specified")])}))


(def aircon-set-temp-param-error-reply
  {:status 400
   :body   (error-reply [(str "'temperature' must be a number from 15 to 30")])})


(defn device-set-state-param-error-reply
  [valid-param]
  {:status 400
   :body   (error-reply [(str "The parameter must be '" valid-param "'")])})


(defn device-set-state-param-value-error-reply
  [valid-param possibles]
  {:status 400
   :body   (error-reply [(str "The parameter '" valid-param "' must be " possibles)])})


(def devices-not-accessible-error-reply
  {:status 403
   :body   (error-reply ["Devices not accessible"])})


(def sensors-not-accessible-error-reply
  {:status 403
   :body   (error-reply ["Sensors not accessible"])})


(def signage-not-accessible-error-reply
  {:status 403
   :body   (error-reply ["Signage not accessible"])})


(defn json-error-reply
  [message]
  {:status 400
   :body   (error-reply [message])})


(def room-num-error-reply
  {:status 400
   :body   (error-reply ["'room_num' must be four digits number"])})


(def card-id-error-reply
  {:status 400
   :body   (error-reply ["'id' must be a number from 1 to 5"])})


(def card-idm-error-reply
  {:status 400
   :body   (error-reply ["'uid' must be 16 digits HEX number"])})


(def locker-open-error-reply
  {:status 503
   :body   (error-reply ["Locker service temporarily unavailable"])})


(defn safe-comment
  "文字列中の制御文字を空白に置換し、両端の空白を除去する

  結果が空文字の場合、スペース1文字を返す。
  これは Web 画面に表示する場合にレイアウトが崩れない、
  JavaScript で偽と解釈されない、など一定のメリットがあるため。"
  [comment]
  (let [c (->> (or comment "")
               (re-seq #"[^\u0000-\u0020\u007F-\u00A0\u00AD\u2000-\u200F\u2028-\u202F\u2060-\u206F\uE000-\uF8FF\uFFF0-\uFFFF]+")
               (str/join " ")
               (str/trim))]
    (if (str/blank? c)
      " "
      c)))


(defn- test-safe-comment
  []
  (let [src (clojure.edn/read-string "\"a\u0000😀b\n\"")]
    (assert (-> (safe-comment "") (= " ")))
    (assert (-> (safe-comment " ") (= " ")))
    (assert (-> (safe-comment " a ") (= "a")))
    (assert (-> (safe-comment "a\n\tあ") (= "a あ")))
    (assert (-> (safe-comment src) (= "a 😀b")))
    true))


(defn room-num
  [param]
  (some-> (:room_num param)
          (as-> s (re-find #"^[0-9]{4}$" s))
          (as-> room
            (if (= room "6000")
              "00001C00000000000002000000480A9C"
              (str "room" room)))))


(defn card-id
  [param]
  (some->> (:card-id param)
           (re-find #"^[1-5]$")
           parse-long))


(defn card-idm
  [param]
  (some-> (get param "uid")
          (as-> s (re-find #"^[a-fA-F0-9]{16}$" s))
          (str/upper-case)))


(def valid-sensor-types
  #{"temperature"
    "humidity"
    "illuminance"
    "pm25"
    "airpressure"
    "pollen"
    "radiation"})


(defn sensor-type-*
  [params]
  (if-let [types (:sensor_type params)]
    (some-> types
            (str/split #"\s+")
            (as-> xs (filter valid-sensor-types xs))
            (seq)
            (as-> xs (map keyword xs))
            (set))
    (-> (map keyword valid-sensor-types)
        (set))))


(defn sensor-type-1
  [params]
  (some-> (:sensor_type params)
          (valid-sensor-types)
          (keyword)))


(def params
  {:name ::params-interceptor
   :enter
   (fn [ctx]
     (-> ctx
         #_(update :request assoc ::xxxxxxxxxxxxxxxxxxxxxxxxxx
                   (select-keys (:request ctx) [:params :path-params]))
         (update :request assoc ::room-num
                 (room-num (get-in ctx [:request :path-params])))
         (update :request assoc ::sensor-type-1
                 (sensor-type-1 (get-in ctx [:request :path-params])))
         (update :request assoc ::sensor-type-*
                 (sensor-type-* (get-in ctx [:request :params])))
         (update :request assoc ::card-id
                 (card-id (get-in ctx [:request :path-params])))
         (update :request assoc ::card-idm
                 (card-idm (get-in ctx [:request :params])))
         (update :request assoc ::proxy-user
                 (get-in ctx [:request :headers "x-edu-user"]))
         (update :request assoc ::proxy-time
                 (get-in ctx [:request :headers "x-edu-time"]))))})


(def not-found-error
  {:name ::not-found-interceptor
   :leave
   (fn [ctx]
     (if (get-in ctx [:response :body])
       ctx
       (-> (assoc ctx :response
                  {:status 404
                   :body   (error-reply ["Not found"])})
           ((:leave http/json-body)))))})


(def json-throwing-interceptor
  (interceptor/interceptor
   {:name  ::json-throwing-interceptor
    :error (fn [ctx ex]
             ;; Here's where you'd handle the exception
             ;; Remember to base your handling decision
             ;; on the ex-data of the exception.
             (let [{:keys [_exception-type exception]} (ex-data ex)]
               (assoc ctx :response
                      (json-error-reply (-> exception
                                            ex-message
                                            clojure.string/split-lines
                                            first
                                            (as-> s (str "Json Parser: " s)))))
               ;; If you cannot handle the exception, re-attach it to the ctx
               ;; using the `:io.pedestal.interceptor.chain/error` key
               #_(assoc ctx ::chain/error ex)))}))


(def app-error-handler
  (error/error-dispatch
   [ctx ex]
   ;; Handle `ArithmeticException`s thrown by `::throwing-interceptor`
   [{:exception-type :com.fasterxml.jackson.core.JsonParseException
     :interceptor    ::json-throwing-interceptor}]
   (assoc ctx :response {:status 500 :body "Exception caught!"})
   :else
   (assoc ctx ::chain/error ex)))
