(ns student.aes
  (:require
   [buddy.core.codecs :as codecs]
   [buddy.core.crypto :as crypto]
   [buddy.core.nonce :as nonce]))


(defn enc
  "`input` に現在時刻(Unix Epoch 秒)と`.`を先頭に10進文字列として加えて
  AES-128-CBC モードで暗号化しHEX文字列として返す。
  暗号化に使用したIVをHEX文字列で先頭に含める。"
  [input secret]
  (let [dat (-> (str (quot (System/currentTimeMillis) 1000)
                     "."
                     input)
                codecs/str->bytes)
        iv  (nonce/random-bytes 16)
        key (codecs/hex->bytes secret)
        eng (crypto/block-cipher :aes :cbc)]
    (str "eduapi.a"
         "."
         (codecs/bytes->hex iv)
         "."
         (-> (crypto/encrypt-cbc eng dat key iv)
             codecs/bytes->hex))))
