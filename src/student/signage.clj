(ns student.signage
  (:require
   [clj-time.core]
   [clj-time.format]
   [clojure.java.io :as io]
   [clojure.set :as set]
   [clojure.string :as str]
   [opencv4.core :as cv]
   [org.httpkit.client :as h]
   [student.ubiq :as ubiq]
   [student.util :as util])
  (:import
   java.io.File
   org.opencv.core.Mat
   (org.opencv.imgcodecs
    Imgcodecs)))


(defn printable-string
  "セパレータもしくは制御文字を消去して綺麗な文字列にする"
  [s]
  (when s
    (-> s
        str/trim
        (str/split #"[\p{Z}\p{Cc}]+")
        (as-> s (str/join " " s)))))


(defn printable-strings
  [seq]
  (map printable-string seq))


(defn has-role
  "ロール集合に一致する要素がある場合 true
  ただし device-roles に * が含まれる場合、全てのロールに許可、の意味とする"
  [device-roles user-roles]
  (let [device-roles (set device-roles)]
    (or
     (contains? device-roles "*")
     (-> (clojure.set/intersection (set user-roles) device-roles)
         empty?
         not))))


(def signage-base "https://edu-iot.iniad.org/api/v1/signages/")
(def singular {"accept" "application/vnd.pgrst.object+json"})
(def merge-dup {"prefer" "resolution=merge-duplicates"})


(def now-str
  (let [fmt (clj-time.format/formatters :date-time)]
    (fn []
      (clj-time.format/unparse fmt (clj-time.core/now)))))


(defn get-priv
  "特権テーブルの取得"
  [table]
  (-> @(h/get (str ubiq/signage-url "_priv")
              {:query-params {"priv_name" (str "eq." table)}
               :headers      {"accept" "application/vnd.pgrst.object+json"}})
      :body
      util/json-read-str
      :groups))


;; (util/safe-comment comment)

(defn list->array
  [items]
  (str "{" (clojure.string/join "," items) "}"))


(defn put-priv
  "特権テーブルの設定"
  [table groups]
  (let [groups (-> groups sort list->array)]
    (-> @(h/put (str ubiq/signage-url "_priv")
                {;; :headers {"prefer" "resolution=merge-duplicates"}
                 :query-params {"priv_name" (str "eq." table)}
                 :body         (util/json-write-str
                                {:priv_name table
                                 :groups    groups})}))))


(defn has-rights
  "サイネージへのアクセス権チェック"
  [rw signage user-roles]
  (-> @(h/get ubiq/signage-url
              {:query-params {"signage_no" (str "eq." signage)
                              "select" (name rw)}
               :headers      singular})
      :body
      util/json-read-str
      rw
      (has-role user-roles)))


(defn list-signage
  "サイネージの一覧"
  []
  (-> @(h/get ubiq/signage-url
              {:query-params {"select" "signage_no"
                              "order"  "signage_no"}
               :headers      {}})
      :body
      util/json-read-str
      (as-> items
        (map (fn [item]
               (str signage-base (:signage_no item)))
             items))))


(defn list-signage-item
  "サイネージアイテムの一覧"
  [signage]
  (-> @(h/get (str ubiq/signage-url "_image")
              {:query-params {"signage_no" (str "eq." signage)
                              "select"     "item_no"
                              "order"      "item_no"}
               :headers      {}})
      :body
      util/json-read-str
      (as-> items
        (map (fn [item]
               (str signage-base signage "/" (:item_no item)))
             items))))


(defn get-signage-image
  "サイネージ画像"
  [signage item]
  (-> @(h/get (str ubiq/signage-url "_image_data")
              {:query-params {"signage_no" (str "eq." signage)
                              "item_no"    (str "eq." item)
                              "select"     "content"}
               :headers      {"accept" "application/octet-stream"}})
      (as-> result
        (let [img (with-open [w (java.io.ByteArrayOutputStream.)]
                    (io/copy (:body result) w)
                    (.toByteArray w))]
          {:status  200
           :headers {"Content-Type" "image/jpeg"}
           :body    img}))))


(defn read-image-at-path
  "path で指定したファイルから画像を読み込む
  画像でない場合などのとき nil を返す"
  [path]
  (when-let [^Mat img (cv/imread path cv/IMREAD_COLOR)]
    (when-not (.empty img)
      img)))


(defn put-signage-image
  "サイネージ画像設定"
  [signage item username ^java.io.File file]
  (if-let [im (-> file .getPath read-image-at-path)]
    (let [temp-file (java.io.File/createTempFile "signage" ".jpg")]
      (try
        (cv/imwrite im (.getPath temp-file))
        (let [data (with-open [r (io/input-stream temp-file)
                               w (java.io.ByteArrayOutputStream.)]
                     (io/copy r w)
                     (-> w
                         .toByteArray
                         javax.xml.bind.DatatypeConverter/printHexBinary
                         (as-> s (str "\\x" s))))]
          (-> @(h/post
                (str ubiq/signage-url "_image_data")
                {:headers merge-dup
                 :body    (util/json-write-str
                           [{:signage_no signage
                             :item_no    item
                             :content    data
                             :created_by username
                             :created_at (now-str)}])})
              (as-> result
                {:status (:status result)
                 :body   (-> result :body)})))
        (finally (.delete temp-file))))
    {:status 400
     :body   "Not an image"}))


(defn signage-item-headers
  [result]
  (if-let [content (-> result :signage_image_data first)]
    {"X-Metadata-Author"      (:created_by result)
     "X-Metadata-Author-Date" (:created_at result)
     "X-Content-Author"       (:created_by content)
     "X-Content-Author-Date"  (:created_at content)}
    {"X-Metadata-Author"      (:created_by result)
     "X-Metadata-Author-Date" (:created_at result)}))


(defn get-signage-item
  "サイネージアイテム"
  [signage item]
  (-> @(h/get (str ubiq/signage-url "_image")
              {:query-params {"signage_no" (str "eq." signage)
                              "item_no"    (str "eq." item)
                              "select"     "title,playtime,created_by,created_at,signage_image_data(created_by,created_at)"}
               :headers      singular})
      (as-> result
        (let [body    (-> result :body util/json-read-str)
              headers (signage-item-headers body)]
          {:status  (:status result)
           :headers headers
           :body    (select-keys body [:title :playtime])}))))


(defn put-signage-item
  "サイネージアイテム設定"
  [signage item username body]
  (-> @(h/post
        (str ubiq/signage-url "_image")
        {:headers merge-dup
         :body    (-> body
                      (conj {:signage_no signage
                             :item_no    item
                             :created_by username
                             :created_at (now-str)})
                      (as-> obj [obj])
                      util/json-write-str)})
      (as-> result
        {:status (:status result)
         :body   (-> result :body util/json-read-str)})))


(defn delete-signage-item
  "サイネージアイテム削除"
  [signage item]
  (-> @(h/delete
        (str ubiq/signage-url "_image")
        {:query-params {"signage_no" (str "eq." signage)
                        "item_no"    (str "eq." item)}
         :headers      singular})
      (as-> result
        {:status (:status result)
         :body   (-> result :body util/json-read-str)})))


(defn get-signage-acc
  "サイネージアクセス権"
  [signage]
  (-> @(h/get ubiq/signage-url
              {:query-params {"signage_no" (str "eq." signage)
                              "select" "read:acc_read,write:acc_write"}
               :headers      singular})
      :body
      util/json-read-str))


(defn put-signage-acc
  "サイネージアクセス権設定"
  [signage readers writers]
  (let [r    (-> readers sort list->array)
        w    (-> writers sort list->array)
        body {:signage_no signage
              :acc_read   r
              :acc_write  w}]
    (-> @(h/put
          ubiq/signage-url
          {:query-params {"signage_no" (str "eq." signage)}
           :headers      singular
           :body         (util/json-write-str body)})
        :body
        ;; util/json-read-str
        )))


(defn put-signage
  "サイネージの追加"
  [signage default-roles]
  (let [acc (-> default-roles sort list->array)]
    (-> @(h/put ubiq/signage-url
                {;; :headers {"prefer" "resolution=merge-duplicates"}
                 :query-params {"signage_no" (str "eq." signage)}
                 :body         (util/json-write-str
                                {:signage_no signage
                                 :acc_read   acc
                                 :acc_write  acc})}))))


(defn delete-signage
  "サイネージの削除"
  [signage]
  (-> @(h/delete
        ubiq/signage-url
        {;; :headers {"prefer" "resolution=merge-duplicates"}
         :query-params {"signage_no" (str "eq." signage)}})))
