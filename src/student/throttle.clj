(ns student.throttle
  (:require
   [clj-time.core :as t]
   [clojure.string :as str]
   [io.pedestal.interceptor.chain :as chain]
   [student.auth :as auth]
   [student.util :as util]))


(defonce history (ref {}))

(defonce cooldown-interval (t/millis 100))


(defn choke-by-name
  [username]
  (let [now  (t/now)
        next (t/plus now cooldown-interval)]
    (some-> (dosync
             (let [last (get @history username)]
               (alter history #(assoc %1 username %2) next)
               last))
            (t/after? now))))


(defn terminate
  [ctx]
  (-> ctx
      (assoc :response
             {:status 503
              :body (util/error-reply
                     ["Too many requests too fast"])})
      (chain/terminate)))


(def throttle-by-username
  {:name ::throttle-by-username-interceptor
   :enter
   (fn [ctx]
     (if (some-> (get-in ctx [:request ::auth/login :username])
                 (str/lower-case)
                 (choke-by-name))
       (terminate ctx)
       ctx))})
