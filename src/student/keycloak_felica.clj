(ns student.keycloak-felica
  (:require
   [org.httpkit.client :as h]
   [student.auth-config :as auth]
   [student.keycloak :as kc]
   [student.util :as util]))


(defn- cards-restructure
  [maps]
  (->> maps
       (map (fn [m]
              {:id      (util/as-number (:cardNum m) 0 1000)
               :uid     (:idm m)
               :comment (:comment m)}))))


(defn- decode-response
  [{:keys [status body]}]
  (let [body (util/json-read-str body)]
    {:status status
     :body   (if (= status 200)
               (cards-restructure body)
               body)}))


(defn list-cards
  [{:keys [username access-token]}]
  @(h/get
    (str auth/oidc-authority "/api/v1/users/" username "/felica")
    {:oauth-token (or access-token (kc/access-token))}
    decode-response))


(defn create-card
  [{:keys [username idm comment]}]
  @(h/post
    (str auth/oidc-authority "/api/v1/users/" username "/felica")
    {:oauth-token (kc/access-token)
     :headers     util/content-type-json
     :body        (util/json-write-str
                   {:idm     idm
                    :comment (util/safe-comment comment)})}
    decode-response))


(defn update-card
  [{:keys [username idm comment]}]
  @(h/put
    (str auth/oidc-authority "/api/v1/users/" username "/felica/" idm)
    {:oauth-token (kc/access-token)
     :headers     util/content-type-json
     :body        (util/json-write-str
                   {:idm     idm
                    :comment (util/safe-comment comment)})}
    decode-response))


(defn delete-card
  [{:keys [username idm]}]
  @(h/delete
    (str auth/oidc-authority "/api/v1/users/" username "/felica/" idm)
    {:oauth-token (kc/access-token)}
    decode-response))
