(ns student.cloudlog
  (:require
   [io.pedestal.log :as log]
   [student.auth :as auth]))


(defn assoc-if
  [coll k v]
  (if v
    (assoc coll k v)
    coll))


(defn alog-if
  [ctx]
  (let [req (:request ctx)
        uri (:uri req)]
    (when (not= uri "/_ah/health")
      (-> {:status       (get-in ctx [:response :status])
           :method       (:request-method req)
           :uri          uri
           :query-params (:query-params req)
           :form-params  (:form-params req)
           :json-params  (:json-params req)
           :username     (get-in req [::auth/login :username])
           :error        (get-in ctx [:response :body :description])
           :x-forwarded-for
           (get-in req [:headers "x-forwarded-for"])}
          (assoc-if :x-proxy-user (:student.util/proxy-user req))
          (assoc-if :x-proxy-time (:student.util/proxy-time req))
          (as-> info
            (log/info :access info))))))


(def cloudlog
  {:name  ::cloudlog-interceptor
   :leave (fn [ctx]
            (alog-if ctx)
            ctx)})
