(ns student.auth-config
  (:require
   [environ.core :refer [env]]
   [org.httpkit.client :as h]
   [student.util :as util]))


(def oidc-authority (or (env :oidc-authority) "https://accounts.iniad.org"))


(def openid-configuration
  (h/get
   (str oidc-authority "/auth/realms/master/.well-known/openid-configuration")
   (fn [{:keys [status body]}]
     (when (= status 200)
       (util/json-read-str body)))))


(def oidc
  {:scope         "openid profile email iniad_profile iniad_staff_profile iniad_stu_profile"
   :client_id     "iniad-portalapp"
   :client_secret "pnyqCygnzBLBELJqXoTcpblCj0eHN5kG"})
