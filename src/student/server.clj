(ns student.server
  (:gen-class)
  ;; for -main method in uberjar
  (:require
   [debug.nrepl :as nrepl]
   [io.pedestal.http :as server]
   [io.pedestal.http.route :as route]
   [io.pedestal.log :as log]
   [student.auth-config]
   [student.service :as service]
   [student.timer :as timer]))


;; This is an adapted service map, that can be started and stopped
;; From the REPL you can call server/start and server/stop on this service
(defonce runnable-service (server/create-server service/service))


(defn run-dev
  "The entry-point for 'lein run-dev'"
  [& _args]
  (println "\nCreating your [DEV] server...")
  ;; 開発中は定時ワーカを起動しない
  ;; (timer/start-time-worker)
  (-> service/service ; start with production configuration
      (merge {:env :dev
              ;; do not block thread that starts web server
              ::server/join? false
              ;; Routes can be a function that resolve routes,
              ;;  we can use this to set the routes to be reloadable
              ::server/routes #(route/expand-routes (deref #'service/routes))
              ;; all origins are allowed in dev mode
              #_ #_
              ::server/allowed-origins {:creds true :allowed-origins (constantly true)}})
      ;; Wire up interceptor chains
      server/default-interceptors
      server/dev-interceptors
      server/create-server
      server/start))


(defn -main
  "The entry-point for 'lein run'"
  [& _args]
  (when (nil? @student.auth-config/openid-configuration)
    (log/error :openid-configuration nil)
    (System/exit 99))
  (log/info :msg "Creating nRepl remote debug server..." :port nrepl/port)
  (nrepl/init)
  (log/info :msg "Creating your server...")
  (timer/start-time-worker)
  (server/start runnable-service))


;; If you package the service up as a WAR,
;; some form of the following function sections is required (for io.pedestal.servlet.ClojureVarServlet).

;; (defonce servlet  (atom nil))
;;
;; (defn servlet-init
;;  [_ config]
;;  ;; Initialize your app here.
;;  (reset! servlet  (server/servlet-init service/service nil)))
;;
;; (defn servlet-service
;;  [_ request response]
;;  (server/servlet-service @servlet request response))
;;
;; (defn servlet-destroy
;;  [_]
;;  (server/servlet-destroy @servlet)
;;  (reset! servlet nil))
