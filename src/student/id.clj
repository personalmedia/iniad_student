(ns student.id
  (:require
   [clojure.string :as str]))


(defn check-digit
  "学籍番号の文字列からチェックデジットを計算"
  [student-id]
  (-> student-id
      seq
      (->> (map int) (apply +))
      (rem 10)))


(defn into-uid
  "学籍番号の文字列をユーザ ID に変換"
  [student-id]
  (str "s"
       (str/lower-case student-id)
       (check-digit student-id)))


(defn inflate-student-id
  "学籍番号の文字列であればチェックデジットを付加してユーザ ID に変換
  そうでない場合には小文字化して返す。

  学籍番号の文字列でない場合、ユーザ ID のチェックデジットの検証はしない。
  学生以外のユーザ ID は小文字化されるだけとなる。"
  [student-id]
  (some-> student-id
          (as-> id
            (or (some-> (re-matches #"(?i)\d[\dA-Z]\d{4}[\dA-Z]{2}\d{2}" id)
                        str/upper-case
                        into-uid)
                (str/lower-case id)))))
