(ns debug.nrepl
  (:require
   [environ.core :refer [env]]
   [nrepl.server
    :refer (start-server stop-server)]))


(defn nrepl-handler
  []
  (require 'cider.nrepl)
  (ns-resolve 'cider.nrepl 'cider-nrepl-handler))


(defonce ^:private __server (atom nil))

(def ^:private set-server! (partial reset! __server))

(def port (try (bigdec (env :nrepl-port)) (catch Exception _ 4001)))


(defn start
  []
  (when-not @__server
    (set-server!
     (start-server :port port :handler (nrepl-handler)))))


(defn stop
  []
  (when-let [server @__server]
    (stop-server server)
    (set-server! nil)))


(defn init
  []
  (start))
