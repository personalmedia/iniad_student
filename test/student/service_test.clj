(ns student.service-test
  (:require
   [clj-time.core :as t]
   [clojure.data.json :refer [read-str]]
   [clojure.test :refer :all]
   [io.pedestal.http :as bootstrap]
   [io.pedestal.test :refer :all]
   [ring.util.codec :as codec]
   [student.service :as service]
   [student.timer :as timer]))


(def service
  (::bootstrap/service-fn (bootstrap/create-servlet service/service)))


(def api-headers
  {"x-forwarded-for" "118.238.221.73, 35.190.18.235"
   "authorization" (str "Basic " (ring.util.codec/base64-encode
                                  (.getBytes "kazuko.kawagoe:iniadteacherpw")))})


(defn drop-value
  [values]
  (if (vector? values)
    (->> values (map drop-value) (into []))
    (update values :value (constantly 0))))


(defn api-response-for
  [service-fn verb url]
  (Thread/sleep 500)
  (-> (response-for service-fn verb url :headers api-headers)
      :body
      (read-str :key-fn keyword)))


(deftest home-page-test
  (is (=
       (-> (response-for service :get "/")
           :body
           (read-str :key-fn keyword))
       {:status "error", :description "Not found"})))


(deftest api-test
  (is (=
       (api-response-for service :get "/api/v1/locker")
       {:name "326503", :floor 3}))
  (is (=
       (api-response-for service :get "/api/v1/smalllocker")
       {:name   "390101"
        :floor  3
        :locked true}))
  (is (=
       (api-response-for service :get "/api/v1/iccards")
       [{:id 1, :uid "01160400A210EB02", :comment "2017-05-31 15:10 waon"}
        {:id 2, :uid "0101031067109306", :comment "2017-03-31 21:00 pasmo"}
        {:id 3, :uid "01010310D90D0D00", :comment "2017-06-05 21:17 debug from browser.😀"}
        {:id 5, :uid "000000000000000E", :comment "2022-07-07 08:06 文字列 123"}]))
  (is (=
       (-> (api-response-for service :get "/api/v1/sensors/3101")
           drop-value)
       [{:room_num "3101", :sensor_type "illuminance", :value 0}
        {:room_num "3101", :sensor_type "humidity", :value 0}
        {:room_num "3101", :sensor_type "airpressure", :value 0}
        {:room_num "3101", :sensor_type "temperature", :value 0}]))
  (is (=
       (-> (api-response-for service :get "/api/v1/sensors/3101/temperature")
           drop-value)
       {:room_num "3101", :sensor_type "temperature", :value 0}))
  (is (=
       (api-response-for service :get "/api/v1/doorlocks/3101")
       ["https://edu-iot.iniad.org/api/v1/doorlocks/3101/1"
        "https://edu-iot.iniad.org/api/v1/doorlocks/3101/2"]))
  (is (=
       (api-response-for service :get "/api/v1/doorlocks/3101/1")
       {:open        false
        :locked      true
        :room_num    "3101"
        :item_num    1
        :device_type "doorlock"}))
  (is (=
       (api-response-for service :get "/api/v1/lights/3101")
       ["https://edu-iot.iniad.org/api/v1/lights/3101/1"
        "https://edu-iot.iniad.org/api/v1/lights/3101/2"]))
  (is (=
       (api-response-for service :get "/api/v1/lights/3101/1")
       {:modes       ["full" "dim" "off"],
        :mode        "full",
        :room_num    "3101",
        :item_num    1,
        :device_type "light"}))
  (is (=
       (api-response-for service :get "/api/v1/aircons/3101")
       ["https://edu-iot.iniad.org/api/v1/aircons/3101/1"
        "https://edu-iot.iniad.org/api/v1/aircons/3101/2"]))
  (is (=
       (api-response-for service :get "/api/v1/aircons/3101/1")
       {:device_type       "aircon"
        :mode              "cool"
        :modes             ["auto" "cool" "heat" "fan"]
        :wind              nil
        :model_type        "OAV"
        :humidity          nil
        :temperature_range nil
        :winds             nil
        :room_num          "3101"
        :on                true
        :humidity_range    nil
        :item_num          1
        :temperature       nil}))
  (is (=
       (api-response-for service :get "/api/v1/aircons/1411/1")
       {:device_type       "aircon"
        :mode              "cool"
        :modes             ["auto" "cool" "heat" "fan"]
        :wind              "high"
        :model_type        "DEX"
        :humidity          40.0
        :temperature_range [15 30]
        :winds             ["auto" "low" "middle" "high"]
        :room_num          "1411"
        :on                true
        :humidity_range    [30 80]
        :item_num          1
        :temperature       24.0}))
  (is (=
       (api-response-for service :get "/api/v1/dooropeners")
       ["https://edu-iot.iniad.org/api/v1/dooropeners/1"
        "https://edu-iot.iniad.org/api/v1/dooropeners/2"
        "https://edu-iot.iniad.org/api/v1/dooropeners/3"
        "https://edu-iot.iniad.org/api/v1/dooropeners/4"
        "https://edu-iot.iniad.org/api/v1/dooropeners/5"
        "https://edu-iot.iniad.org/api/v1/dooropeners/9"
        "https://edu-iot.iniad.org/api/v1/dooropeners/6"
        "https://edu-iot.iniad.org/api/v1/dooropeners/7"
        "https://edu-iot.iniad.org/api/v1/dooropeners/8"]))
  (is (=
       (api-response-for service :get "/api/v1/dooropeners/1")
       {:name        "自動扉DOOROPENER01"
        :item_num    1
        :device_type "dooropener"
        :status      "stopped"}))
  (is (=
       (api-response-for service :get "/api/v1/signages")
       ["https://edu-iot.iniad.org/api/v1/signages/1000"
        "https://edu-iot.iniad.org/api/v1/signages/1001"]))
  (is (=
       (api-response-for service :get "/api/v1/signages/1000")
       ["https://edu-iot.iniad.org/api/v1/signages/1000/1"]))
  (is (=
       (api-response-for service :get "/api/v1/signages/1000/1")
       {:title    "my title"
        :playtime 30}))
  (is (=
       (api-response-for service :get "/api/v1/signages/1000/role")
       {:read  ["*"]
        :write ["facilityAdmins" "student"]})))


(deftest timer-test
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 21 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 24)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 22 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 27)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 23 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 27)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 24 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 27)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 25 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 30)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 26 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 30)))
  (is (=
       (timer/quantum-time
        (t/plus (t/date-time 2017 07 19 16 27 59) (t/seconds 120)) 3)
       (t/date-time 2017 07 19 16 30))))


#_
(deftest home-page-test
  (is (=
       (:body (response-for service :get "/"))
       "Hello World!"))
  (is (=
       (:headers (response-for service :get "/"))
       {"Content-Type" "text/html;charset=UTF-8"
        "Strict-Transport-Security" "max-age=31536000; includeSubdomains"
        "X-Frame-Options" "DENY"
        "X-Content-Type-Options" "nosniff"
        "X-XSS-Protection" "1; mode=block"
        "X-Download-Options" "noopen"
        "X-Permitted-Cross-Domain-Policies" "none"
        "Content-Security-Policy" "object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;"})))


#_
(deftest about-page-test
  (is (.contains
       (:body (response-for service :get "/about"))
       "Clojure 1.8"))
  (is (=
       (:headers (response-for service :get "/about"))
       {"Content-Type" "text/html;charset=UTF-8"
        "Strict-Transport-Security" "max-age=31536000; includeSubdomains"
        "X-Frame-Options" "DENY"
        "X-Content-Type-Options" "nosniff"
        "X-XSS-Protection" "1; mode=block"
        "X-Download-Options" "noopen"
        "X-Permitted-Cross-Domain-Policies" "none"
        "Content-Security-Policy" "object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;"})))
