(ns student.function-test
  (:require
   [clojure.test :refer :all]
   [student.id :as id]))


(deftest id-test
  (is (= (id/check-digit "0000000000") 0))
  (is (= (id/check-digit "1000000000") 1))
  (is (= (id/check-digit "3F10230002") 3))
  (is (= (id/check-digit "1F10180026") 1))

  (is (= (id/inflate-student-id "3F10230002") "s3f102300023"))
  (is (= (id/inflate-student-id "3f10230002") "s3f102300023"))
  (is (= (id/inflate-student-id "s3f10230002") "s3f10230002"))
  (is (= (id/inflate-student-id "s3f102300023") "s3f102300023"))

  (is (= (id/inflate-student-id "kazuko.Kawagoe") "kazuko.kawagoe"))
  (is (= (id/inflate-student-id "") ""))
  (is (= (id/inflate-student-id nil) nil)))
