# 認証サーバの切り替えにともなう動作確認

U=username:password
OLD=https://edu-iot.iniad.org
NEW=http://localhost:8080

---
% curl -u $U $OLD/api/v1/locker -s|jq
{
  "name": "326503",
  "floor": 3
}
% curl -u $U $NEW/api/v1/locker -s|jq
{
  "name": "326503",
  "floor": 3
}

---
% curl -u $U $OLD/api/v1/pclocker -s|jq
{
  "status": "error",
  "description": "Not found"
}
% curl -u $U $NEW/api/v1/pclocker -s|jq
{
  "status": "error",
  "description": "Not found"
}

---
% curl -u $U $OLD/api/v1/smalllocker -s|jq
{
  "name": "390101",
  "floor": 3,
  "locked": true
}
% curl -u $U $NEW/api/v1/smalllocker -s|jq
{
  "name": "390101",
  "floor": 3,
  "locked": true
}

---
% curl -u $U $OLD/api/v1/iccards -s|jq
[
  {
    "id": 1,
    "uid": "01160400A210EB02",
    "comment": "2017-05-31 15:10 waon"
  },
  {
    "id": 2,
    "uid": "0101031067109306",
    "comment": "2017-03-31 21:00 pasmo"
  },
  {
    "id": 3,
    "uid": "01010310D90D0D00",
    "comment": "2017-06-05 21:17 debug ok."
  }
]
% curl -u $U $NEW/api/v1/iccards -s|jq
[
  {
    "id": 1,
    "uid": "01160400A210EB02",
    "comment": "2017-05-31 15:10 waon"
  },
  {
    "id": 2,
    "uid": "0101031067109306",
    "comment": "2017-03-31 21:00 pasmo"
  },
  {
    "id": 3,
    "uid": "01010310D90D0D00",
    "comment": "2017-06-05 21:17 debug ok."
  }
]

---
% curl -u $U $OLD/api/v1/sensors/3101 -s|jq
[
  {
    "room_num": "3101",
    "sensor_type": "illuminance",
    "value": 282
  },
  {
    "room_num": "3101",
    "sensor_type": "humidity",
    "value": 59.5
  },
  {
    "room_num": "3101",
    "sensor_type": "airpressure",
    "value": 993.5
  },
  {
    "room_num": "3101",
    "sensor_type": "temperature",
    "value": 25.7
  }
]
% curl -u $U $NEW/api/v1/sensors/3101 -s|jq
[
  {
    "room_num": "3101",
    "sensor_type": "illuminance",
    "value": 282
  },
  {
    "room_num": "3101",
    "sensor_type": "humidity",
    "value": 59.5
  },
  {
    "room_num": "3101",
    "sensor_type": "airpressure",
    "value": 993.5
  },
  {
    "room_num": "3101",
    "sensor_type": "temperature",
    "value": 25.7
  }
]

---
% curl -u $U $OLD/api/v1/sensors/3101/humidity -s|jq
{
  "room_num": "3101",
  "sensor_type": "humidity",
  "value": 59.5
}
% curl -u $U $NEW/api/v1/sensors/3101/humidity -s|jq
{
  "room_num": "3101",
  "sensor_type": "humidity",
  "value": 59.5
}

---
% curl -u $U $OLD/api/v1/doorlocks/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/doorlocks/3101/1",
  "https://edu-iot.iniad.org/api/v1/doorlocks/3101/2"
]
% curl -u $U $NEW/api/v1/doorlocks/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/doorlocks/3101/1",
  "https://edu-iot.iniad.org/api/v1/doorlocks/3101/2"
]

---
% curl -u $U $OLD/api/v1/doorlocks/3101/1 -s|jq
{
  "open": true,
  "locked": false,
  "room_num": "3101",
  "item_num": 1,
  "device_type": "doorlock"
}
% curl -u $U $NEW/api/v1/doorlocks/3101/1 -s|jq
{
  "open": true,
  "locked": false,
  "room_num": "3101",
  "item_num": 1,
  "device_type": "doorlock"
}

---
% curl -u $U $OLD/api/v1/lights/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/lights/3101/1",
  "https://edu-iot.iniad.org/api/v1/lights/3101/2"
]
% curl -u $U $NEW/api/v1/lights/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/lights/3101/1",
  "https://edu-iot.iniad.org/api/v1/lights/3101/2"
]

---
% curl -u $U $OLD/api/v1/lights/3101/1 -s|jq
{
  "modes": [
    "full",
    "dim",
    "off"
  ],
  "mode": "full",
  "room_num": "3101",
  "item_num": 1,
  "device_type": "light"
}
% curl -u $U $NEW/api/v1/lights/3101/1 -s|jq
{
  "modes": [
    "full",
    "dim",
    "off"
  ],
  "mode": "full",
  "room_num": "3101",
  "item_num": 1,
  "device_type": "light"
}

---
% curl -u $U $OLD/api/v1/aircons/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/aircons/3101/1",
  "https://edu-iot.iniad.org/api/v1/aircons/3101/2"
]
% curl -u $U $NEW/api/v1/aircons/3101 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/aircons/3101/1",
  "https://edu-iot.iniad.org/api/v1/aircons/3101/2"
]

---
% curl -u $U $OLD/api/v1/aircons/3101/1 -s|jq
{
  "device_type": "aircon",
  "mode": "fan",
  "modes": [
    "auto",
    "cool",
    "heat",
    "fan"
  ],
  "wind": null,
  "model_type": "OAV",
  "humidity": null,
  "temperature_range": null,
  "winds": null,
  "room_num": "3101",
  "on": true,
  "humidity_range": null,
  "item_num": 1,
  "temperature": null
}
% curl -u $U $NEW/api/v1/aircons/3101/1 -s|jq
{
  "device_type": "aircon",
  "mode": "fan",
  "modes": [
    "auto",
    "cool",
    "heat",
    "fan"
  ],
  "wind": null,
  "model_type": "OAV",
  "humidity": null,
  "temperature_range": null,
  "winds": null,
  "room_num": "3101",
  "on": true,
  "humidity_range": null,
  "item_num": 1,
  "temperature": null
}

---
% curl -u $U $OLD/api/v1/iotswitches -s|jq
{
  "status": "error",
  "description": "Not found"
}
% curl -u $U $NEW/api/v1/iotswitches -s|jq
{
  "status": "error",
  "description": "Not found"
}

---
% curl -u $U $OLD/api/v1/dooropeners -s|jq
[
  "https://edu-iot.iniad.org/api/v1/dooropeners/1",
  "https://edu-iot.iniad.org/api/v1/dooropeners/2",
  "https://edu-iot.iniad.org/api/v1/dooropeners/3",
  "https://edu-iot.iniad.org/api/v1/dooropeners/4",
  "https://edu-iot.iniad.org/api/v1/dooropeners/5",
  "https://edu-iot.iniad.org/api/v1/dooropeners/6",
  "https://edu-iot.iniad.org/api/v1/dooropeners/7",
  "https://edu-iot.iniad.org/api/v1/dooropeners/8"
]
% curl -u $U $NEW/api/v1/dooropeners -s|jq
[
  "https://edu-iot.iniad.org/api/v1/dooropeners/1",
  "https://edu-iot.iniad.org/api/v1/dooropeners/2",
  "https://edu-iot.iniad.org/api/v1/dooropeners/3",
  "https://edu-iot.iniad.org/api/v1/dooropeners/4",
  "https://edu-iot.iniad.org/api/v1/dooropeners/5",
  "https://edu-iot.iniad.org/api/v1/dooropeners/6",
  "https://edu-iot.iniad.org/api/v1/dooropeners/7",
  "https://edu-iot.iniad.org/api/v1/dooropeners/8"
]

---
% curl -u $U $OLD/api/v1/dooropeners/1 -s|jq
{
  "name": "自動扉DOOROPENER01",
  "item_num": 1,
  "device_type": "dooropener"
}
% curl -u $U $NEW/api/v1/dooropeners/1 -s|jq
{
  "name": "自動扉DOOROPENER01",
  "item_num": 1,
  "device_type": "dooropener"
}

---
% curl -u $U $OLD/api/v1/signages -s|jq
[
  "https://edu-iot.iniad.org/api/v1/signages/1000",
  "https://edu-iot.iniad.org/api/v1/signages/1001"
]
% curl -u $U $NEW/api/v1/signages -s|jq
[
  "https://edu-iot.iniad.org/api/v1/signages/1000",
  "https://edu-iot.iniad.org/api/v1/signages/1001"
]

---
% curl -u $U $OLD/api/v1/signages/1000 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/signages/1000/1"
]
% curl -u $U $NEW/api/v1/signages/1000 -s|jq
[
  "https://edu-iot.iniad.org/api/v1/signages/1000/1"
]

---
% curl -u $U $OLD/api/v1/signages/1000/1 -s|jq
{
  "title": "my title",
  "playtime": 30
}
% curl -u $U $NEW/api/v1/signages/1000/1 -s|jq
{
  "title": "my title",
  "playtime": 30
}

---
% curl -u $U $OLD/api/v1/signages/1000/role -s|jq
{
  "read": [
    "*"
  ],
  "write": [
    "facilityAdmins",
    "student"
  ]
}
% curl -u $U $NEW/api/v1/signages/1000/role -s|jq
{
  "read": [
    "*"
  ],
  "write": [
    "facilityAdmins",
    "student"
  ]
}

---
% curl -u $U $OLD/api/v1/accessibility/3101 -G -d proxy_user=s99121700013 -d proxy_time=2018-02-01T1030 -s|jq
[
  {
    "device_type": "sensor",
    "ucode": "00001C000000000000020000002D213B",
    "symbol": "S3054",
    "name": "センサーS3054",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": null,
    "accessible": false
  },
  {
    "device_type": "sensor",
    "ucode": "00001C000000000000020000002D213C",
    "symbol": "S3055",
    "name": "センサーS3055",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": null,
    "accessible": false
  },
  {
    "device_type": "aircon",
    "ucode": "00001C00000000000002000000480712",
    "symbol": "OAV3021",
    "name": "換気・空調OAV3021",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "aircon",
    "ucode": "00001C00000000000002000000480615",
    "symbol": "PAC3047",
    "name": "空調PAC3047",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  },
  {
    "device_type": "doorlock",
    "ucode": "00001C00000000000002000000480591",
    "symbol": "D3009",
    "name": "電気錠D3009",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "doorlock",
    "ucode": "00001C000000000000020000004805BB",
    "symbol": "D3010",
    "name": "電気錠(3101教室と3105教室の間)D3010",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  },
  {
    "device_type": "light",
    "ucode": "00001C000000000000020000004807AB",
    "symbol": "LPFDO1963",
    "name": "パターン照明LPFDO1963",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "light",
    "ucode": "00001C00000000000002000000480825",
    "symbol": "LU2085",
    "name": "照明LU2085",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  }
]
% curl -u $U $NEW/api/v1/accessibility/3101 -G -d proxy_user=s99121700013 -d proxy_time=2018-02-01T1030 -s|jq
[
  {
    "device_type": "sensor",
    "ucode": "00001C000000000000020000002D213B",
    "symbol": "S3054",
    "name": "センサーS3054",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": null,
    "accessible": false
  },
  {
    "device_type": "sensor",
    "ucode": "00001C000000000000020000002D213C",
    "symbol": "S3055",
    "name": "センサーS3055",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": null,
    "accessible": false
  },
  {
    "device_type": "aircon",
    "ucode": "00001C00000000000002000000480712",
    "symbol": "OAV3021",
    "name": "換気・空調OAV3021",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "aircon",
    "ucode": "00001C00000000000002000000480615",
    "symbol": "PAC3047",
    "name": "空調PAC3047",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  },
  {
    "device_type": "doorlock",
    "ucode": "00001C00000000000002000000480591",
    "symbol": "D3009",
    "name": "電気錠D3009",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "doorlock",
    "ucode": "00001C000000000000020000004805BB",
    "symbol": "D3010",
    "name": "電気錠(3101教室と3105教室の間)D3010",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  },
  {
    "device_type": "light",
    "ucode": "00001C000000000000020000004807AB",
    "symbol": "LPFDO1963",
    "name": "パターン照明LPFDO1963",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 1,
    "accessible": false
  },
  {
    "device_type": "light",
    "ucode": "00001C00000000000002000000480825",
    "symbol": "LU2085",
    "name": "照明LU2085",
    "proxy_user": "s99121700013",
    "proxy_time": "2018-02-01T1030",
    "room_num": "3101",
    "item_num": 2,
    "accessible": false
  }
]

---
