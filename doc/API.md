# INIAD 教育用 API

## 基本情報

- 本仕様のバージョン
  - 2023/10/30
- 改訂履歴
  - 2023/10/30 aircons の更新パラメタが on: false のときパラメタ無しとして処理していたバグを修正。
  - 2023/08/02 ubiq エンドポイントの追加。
  - 2020/12/08 aircons, doorlocks, lights, iccards の更新パラメータとして JSON を受理。
  - 2020/04/01 自動ドア、サイネージ用の API を追加。
  - 2019/02/26 PC ロッカー用、小物ロッカー用の API を追加。
  - 2018/04/04 空調の制御対象を PAC 以外のすべてのモデルに拡張。
  - 2018/02/16 電気錠、照明、空調の状態取得・変更 API の追加。
  - 2018/02/16 アクセス権限の状態取得 API の追加。
  - 2018/02/16 照明デバイス番号の状態取得・変更 API の追加。
  - 2018/01/12 API のホスト名を実際のデプロイ状況にあわせて修正。
  - 2017/07/14 応答コードの追加変更。ロッカー ACL 更新について記載。
  - 2017/07/04 初版

## URL

- プロトコル
  - https
- ホスト名
  - edu-iot.iniad.org
- ベースパス
  - /api/v1

## 認証

- 方針
  - ユーザー名・パスワードを利用して認証する
  - Basic 認証を利用する

## API の一覧と受付メソッド

- /locker
  - GET: 自分に紐付いたロッカーの情報を取得する
- /locker/open
  - POST: 自分に紐付いたロッカーを開ける
- /pclocker
  - GET: 自分に紐付いた PC ロッカーの情報を取得する
- /pclocker/open
  - POST: 自分に紐付いた PC ロッカーを開ける
- /smalllocker
  - GET: 自分に紐付いた小物ロッカーの情報を取得する
- /smalllocker/open
  - POST: 自分に紐付いた小物ロッカーを開ける
- /iccards
  - GET: 自分に紐付いたカードのリストを取得
  - POST: カードを登録する。なお現時点ではこのカード情報はロッカーを開けるのに利用する
- /iccards/{id}
  - GET: 指定した id を持つカードを取得（id は 1,2,3 とシステム内部で付与した ID）
  - PUT/PATCH: 指定した id を持つカードの情報を更新する
  - DELETE: 指定した id を持つカードを削除する
- /sensors/{room_num}
  - GET: 指定した部屋のセンサーのリストとその直近の読み取り値を返す
- /sensors/{room_num}/{sensor_type}

  - GET: 指定した部屋の指定したセンサーの読み取り値を返す

- /doorlocks/{room_num}

  - GET: 指定した部屋(room_num)の電気錠の URL を一覧で返す

- /doorlocks/{room_num}/{item_number}

  - GET: 指定した部屋(room_num)の指定した番号(item_number)の電気錠の読み取り値を返す
  - PUT: 指定した部屋(room_num)の指定した番号(item_number)の電気錠を開錠・施錠する

- /lights/{room_num}

  - GET: 指定した部屋(room_num)の照明の URL を一覧で返す

- /lights/{room_num}/{item_number}

  - GET: 指定した部屋(room_num)の指定した番号(item_number)の照明の読み取り値を返す
  - PUT: 指定した部屋(room_num)の指定した番号(item_number)の照明を変更する

- /aircons/{room_rum}

  - GET: 指定した部屋(room_num)の空調の URL を一覧で返す

- /aircons/{room_rum}/{item_number}

  - GET: 指定した部屋(room_num)の指定した番号(item_number)の空調の読み取り値を返す
  - PUT: 指定した部屋(room_num)の指定した番号(item_number)の空調を変更する
  - 電源、温度、暖房冷房、風量を変更する

- /iotswitches

  - GET: IoT スイッチへのアクセストークン取得用の URL を一覧で返す

- /iotswitches/{item_number}

  - GET: {item_number}で指定した番号の IoT スイッチへのアクセストークンを返す
  - トークンには有効期限があるので、一度取得して永続的に使用するのではなく、期限が切れたら再度取得しなおす

- /dooropeners
  - GET: 存在する自動ドアの URL のリストを返す
- /dooropeners/{item_num}

  - GET: 指定した番号(item_num)の自動ドアの情報を取得する
  - POST: 指定した番号(item_num)の自動ドアを一定時間オープンする

- /signages

  - GET: 存在するサイネージ機器の URL のリストを返す
    - サイネージ機器は signage_num によって識別される。

- /signages/{signage_num}

  - GET: signage_num で指定した機器のサイネージに登録された、サイネージ画像の関連情報の URL のリストを返す

    - サイネージ画像は item_num の番号順に、指定された時間だけ再生(表示)される。番号が最大のサイネージ画像を再生した後は、番号が最小のサイネージ画像に戻り、これを繰り返す。item_num は POST や PUT のパラメータとしてアプリケーション側で指定する。番号には欠番があってもよく、連続とは限らない。
    - 表示するそれぞれのサイネージ画像に対しては、関連情報(タイトル、再生秒数)と画像データを設定する必要がある。関連情報と画像データの両方が設定されているサイネージ画像のみが再生される。
    - この API では、関連情報と画像データの少なくとも一方が設定されているサイネージ画像の URL のリストを返す。関連情報と画像データの一方のみ設定されたサイネージ画像がある場合は、この API で得られるリストと実際に再生されるサイネージ画像が一致しない。

  - PUT: signage_num で指定したサイネージ機器をリストに追加する

    - この API は、教員の OpenAM 権限グループ(facilityAdmins, fullTimeTeachers, partTimeTeachers, graduateSchoolTeachers)に所属しているユーザのみが実行できる

  - DELETE: signage_num で指定したサイネージ機器をリストから削除する
    - この API は、教員の OpenAM 権限グループ(facilityAdmins, fullTimeTeachers, partTimeTeachers, graduateSchoolTeachers)に所属しているユーザのみが実行できる

- /signages/{signage_num}/{item_num}

  - GET: signage_num で指定した機器のサイネージで表示される、item_num の番号に対応するサイネージ画像の関連情報を取得する
    - サイネージ画像のタイトル(title)と再生秒数(playtime)を返す
  - PUT: signage_num で指定した機器のサイネージで表示される、item_num の番号に対応するサイネージ画像の関連情報を設定する
    - サイネージ画像のタイトル(title)と再生秒数(playtime)をパラメータとする
  - DELETE: signage_num で指定した機器のサイネージで表示される、item_num の番号に対応するサイネージ画像の画像データと関連情報を削除する。item_num は欠番となる

- /signages/{signage_num}/{item_num}/data

  - GET: signage_num で指定した機器のサイネージで表示される、item_num の番号に対応するサイネージ画像の画像データを取得する
    - 画像ファイルのバイナリデータ(application/octet-stream)を返す
  - POST: signage_num で指定した機器のサイネージで表示される、item_num の番号に対応するサイネージ画像の画像データを設定する
    - 画像ファイルのマルチパート形式(multipart/form-data, name=file)をリクエストボディーとする
    - ファイルパートのデータは JPEG, PNG のいずれかの画像フォーマットであること
    - ファイルパートのデータサイズは 1MB 以下であること

- /signages/{signage_num}/role

  - GET: signage_num で指定した機器のサイネージのアクセス権限を取得する。この API は誰でも実行できる
    - 読み出しアクセス権のある権限グループのリスト(read)と書き込みアクセス権のある権限グループのリスト(write)を返す
  - PUT: signage_num で指定した機器のサイネージのアクセス権限を設定する。この API は教員の OpenAM 権限グループ(facilityAdmins, fullTimeTeachers, partTimeTeachers, graduateSchoolTeachers) に所属しているユーザのみが実行できる。
    - 読み出しアクセス権のある権限グループのリスト(read)と書き込みアクセス権のある権限グループのリスト(write)をパラメータとする

- /ubiq/{\*path}

  - ユビキタスサーバへリクエストを転送します。
  - ユビキタスサーバからのレスポンスをそのまま返します。
  - 他のエンドポイントと異なり、デバイスの識別には ucode を使用します。(またはデバイスシンボル)
  - `path` はユビキタスサーバの API として有効でなければなりません。
  - 使用可能な `path` については、ユビキタスサーバの API 仕様書を参照してください。

ucode で指定した電気錠を開錠する例:

    POST /api/v1/ubiq/doorlocks/00001C00000000000002000000480D81/unlock

デバイスシンボルで指定した電気錠を開錠する例:

    POST /api/v1/ubiq/doorlocks/D5058/unlock

既存の教育 API 体系(\*)と違い対象のデバイスを直接 ID で指定しますので、部屋に属していない、たとえば廊下の電気錠も開錠できることになります。

\* 既存の教育 API 体系では、デバイスは (部屋番号、インデックス番号) の組であらわされます。

### 管理用 API

#### 照明の番号割り当て変更

- 照明の番号を XY で座標のように割り当てたい、といった要望に対応するため、割り当て番号をいつでも変更できるようにする。

  - 建物の設計図等からは個々の照明の具体的位置までわからないため、番号割り当てを現場対応していただくための API。
  - 学生が利用することは想定していない。

- /lights/{room_rum}/{item_number}/renumber

  - PUT: 指定した部屋(room_num)の指定した番号(item_number)の照明の番号を変更する
  - Form param
    - to: number 新しい番号
  - 重複する番号は指定できない。
  - 0 < number < 1000 とする。

- 実行例

```
curl https://edu-iot.iniad.org/api/v1/lights/5555/1/renumber -d to=99 -X PUT -u your_user:your_pass
```

#### ユーザ ID・時刻を指定したアクセス権調査

- あるユーザの権限・ある指定時刻で API を実行した場合にどのような結果になるのか(アクセス権のありなし等)を、先生が知るための機能。

- ユーザ ID・パスワードの指定方法に変更はなく、API をアクセスする本人(先生)の ID・パスワードを指定する。
- 先生のロールを持つアカウントでのみ機能する。

##### ユーザ ID の指定(必須)

- クエリパラメータ `proxy_user` の値として任意のユーザ ID を指定する。
- 指定されたユーザが正当にログインしてアクセスしているものとして、ユビキタスサーバに問い合わせ(HTTP 発行)をおこなう。

##### 実行時刻の指定(オプショナル)

- クエリパラメータ `proxy_time` の値として任意の時刻を指定する。
- 指定されたユーザが指定時刻にログインしてアクセスしているものとして、ユビキタスサーバに問い合わせ(HTTP 発行)をおこなう。
  - 一般的には先の `proxy_user` とあわせて指定することで、指定ユーザが指定時刻にアクセス権を持つかどうかのチェックに使う。
- 時刻のフォーマットは以下を受理する。
  - 例: 2018-02-01T1030
  - (4 桁の西暦)-(2 桁の月)-(2 桁の日)(アルファベットの T)(2 桁の時)(2 桁の分)
  - タイムゾーンは常に日本標準時として解釈される
  - クエリパラメータに使える文字種の制限のため、時刻にコロンは使用しない。

##### API

部屋に含まれるデバイスのアクセス権(Accessible)の一覧を返す

- GET /accessibility/{room_num}

  - room_num: 設置されている部屋の番号（文字列）。屋上の場合は 6000。IoT スイッチの場合は 9999。

- 実行例

```
curl https://edu-iot.iniad.org/api/v1/accessibility/5555 -G -d proxy_user=s99121700013 -d proxy_time=2018-02-01T1030 -u your_user:your_pass
```

## モデル

- Locker
  - floor: ロッカーのフロアを返す
  - name: 次のルールで名付けたロッカーの番地を 10 進数 6 桁の数字からなる文字列で返す（以下、上位桁から順に記載）
    - フロア番号 1 桁
    - ストリート番号 1 桁
    - 列番号 2 桁（教室番号と同じ向きで若い番号にする=若いストリートに面している側が若い列番号）
    - 行番号 2 桁（上を若い番号にする）
    - （例: 320104: 3 階の 2 ストリートにあるロッカーの 1 列目の上から 4 段目）
- PcLocker
  - floor: PC ロッカーのフロアを返す
  - name: Locker と同様のルールで名付けた PC ロッカーの番地を 10 進数 6 桁の数字からなる文字列で返す
  - locked: 施錠状態(true=施錠)
  - occupied: 荷物の有無(true=有り)
- SmallLocker
  - floor: 小物ロッカーのフロアを返す
  - name: Locker と同様のルールで名付けた小物ロッカーの番地を 10 進数 6 桁の数字からなる文字列で返す
  - locked: 施錠状態(true=施錠)
- ICCard
  - id: ユーザーのもつ IC カードの通し番号（システム内でユニークではなく、ユーザー内でユニークとする。1 から 5 の数字）。通し番号はリクエストの度にかわらないようにする。
  - comment: カードに関するわかりやすい自分用の説明
  - uid: カードの固有識別子
- Sensor
  - room_num: 設置されている部屋の番号（文字列）。屋上の場合は 6000 を指定する。
  - sensor_type: temperature, humidity, illuminance, pm25, airpressure, pollen, radiation
  - value:読み取り値
    - 複数のセンサーのうちアクセス権のあるものの平均値を返す
- Error

  - status: "error"を入れる
  - description: エラーの詳細を言葉で入れる

- Aircon

  - on: bool 電源の状態(true=電源が入っている)
  - mode: string 運転モード
    - modes のうち、いづれかの値となる。
  - modes: string list readonly
    - mode が取り得る値の種類をあらわす。
    - 空調デバイスの種類(model_type)によって変化する。
  - wind: string 風量
    - winds のうち、いづれかの値となる。
  - winds: string list readonly
    - wind が取り得る値の種類をあらわす。
    - 空調デバイスの種類(model_type)によって変化する。
  - temperature: number 設定温度(摂氏、1℃ きざみの整数)
  - temperature_range: number list readonly
    - temperature の下限と上限の値をあらわす。
  - humidity: number 設定湿度(摂氏、1℃ きざみの整数)
  - humidity_range: number list readonly
    - humidity の下限と上限の値をあらわす。
  - room_num: string readonly 設置されている部屋の番号（文字列）
  - item_num: number readonly 設置されている部屋におけるデバイス番号
  - device_type: string readonly "aircon"
  - model_type: string readonly
    - 空調デバイスのモデル
    - DEX, MHU, OAP, OAV, PAC, VAM, VEN, VRV のいづれかとなる。
  - 空調デバイスのモデルにおうじて、無効な属性は null 値が格納される。
    - mode, modes, wind, winds, temperature, temperature_range, humidity, humidity_range が null になり得る。

- Doorlock

  - open: bool readonly ドアの開閉状態(true=開いている)
  - locked: bool ドア錠の施錠状態(true=施錠されている)
  - room_num: string readonly 設置されている部屋の番号（文字列）
  - item_num: number readonly 設置されている部屋におけるデバイス番号
  - device_type: string readonly "doorlock"

- Dooropener

  - item_num: number readonly 設置されている場所におけるデバイス番号
  - name: string readonly 説明用の名称（別途定義する）
  - device_type: string readonly "dooropener"

- Light

  - mode: string
    - 照明の現在状態を示す。
    - modes のうち、いづれかの値となる。
  - modes: string list readonly
    - mode が取り得る値の種類をあらわす。
    - 照明デバイスの種類によって変化する。
    - 以下のうちひとつ以上を含む。
      - full: 全点灯
      - off: 消灯
      - dim: 暗め
      - lv6, lv5, lv4, lv3, lv2, lv1: 調光式照明のレベル
  - room_num: string readonly 設置されている部屋の番号（文字列）。屋上の場合は 6000。
  - item_num: number readonly 設置されている部屋におけるデバイス番号
  - device_type: string readonly "light"

- IoTswitchToken (readonly)

  - url: string 接続すべき Notification Broker (WAMP サーバ)の URL
  - realm: string WAMP プロトコルで接続するときの realm
  - topic: string WAMP プロトコルで接続するときの topic
  - subprotocol: string "wamp.2.json"
  - item_num: number デバイス番号
  - device_type: string "iotswitch"

- Accessible (readonly)
  - ucode: string デバイス ucode
  - symbol: string デバイスシンボル
  - name: string デバイス名
  - device_type: string (aircon, doorlock, light, sensor, iotswitch)
  - room_num: string 設置されている部屋の番号（文字列）。屋上の場合は 6000。IoT スイッチの場合は 9999。
  - item_num: number nullable 設置されている部屋におけるデバイス番号
    - センサーデバイスのみ null になる。センサーの状態取得 API において、デバイス個別に識別していないので、この情報を持たないため。
  - proxy_user: string アクセス権調査対象としたユーザアカウント
  - proxy_time: string アクセス権調査対象とした時刻
  - accessible: bool アクセス権の有無(true=有)
    - アクセス権がある場合、状態取得と状態変更のどちらも可能である。

## 参考出力例

```
https://edu-iot.iniad.org/api/v1/aircons/4213
[
  "https://edu-iot.iniad.org/api/v1/aircons/4213/1"
]

https://edu-iot.iniad.org/api/v1/aircons/4213/1
{
  "device_type": "aircon",
  "mode": "heat",
  "modes": [
    "auto",
    "cool",
    "heat",
    "fan",
    "dry"
  ],
  "wind": "auto",
  "model_type": "PAC",
  "humidity": null,
  "temperature_range": [
    15,
    30
  ],
  "winds": [
    "auto",
    "low",
    "high"
  ],
  "room_num": "4302",
  "on": false,
  "humidity_range": null,
  "item_num": 2,
  "temperature": 26
}

https://edu-iot.iniad.org/api/v1/doorlocks/4213
[
  "https://edu-iot.iniad.org/api/v1/doorlocks/4213/1"
]

https://edu-iot.iniad.org/api/v1/doorlocks/4213/1
{
  "open": false,
  "locked": true,
  "room_num": "4213",
  "item_num": 1,
  "device_type": "doorlock"
}

https://edu-iot.iniad.org/api/v1/lights/4213
[
  "https://edu-iot.iniad.org/api/v1/lights/4213/1",
  "https://edu-iot.iniad.org/api/v1/lights/4213/2"
]

https://edu-iot.iniad.org/api/v1/lights/4213/1
{
  "modes": [
    "full",
    "dim",
    "off"
  ],
  "mode": "off",
  "room_num": "4213",
  "item_num": 1,
  "device_type": "light"
}

https://edu-iot.iniad.org/api/v1/iotswitches
[
  "https://edu-iot.iniad.org/api/v1/iotswitches/1",
  "https://edu-iot.iniad.org/api/v1/iotswitches/2",
  "https://edu-iot.iniad.org/api/v1/iotswitches/3",
  "https://edu-iot.iniad.org/api/v1/iotswitches/4"
]

https://edu-iot.iniad.org/api/v1/iotswitches/1
{
  "url": "wss://portal.iniad.org:8201/",
  "realm": "eduapi.a.b6b5960d2...",
  "topic": "get.devices.iotswitch.ABC0123...",
  "subprotocol": "wamp.2.json",
  "item_num": 1,
  "device_type": "iotswitch"
}

https://edu-iot.iniad.org/api/v1/dooropeners
[
  "https://edu-iot.iniad.org/api/v1/dooropeners/1",
  "https://edu-iot.iniad.org/api/v1/dooropeners/2",
  "https://edu-iot.iniad.org/api/v1/dooropeners/3",
  "https://edu-iot.iniad.org/api/v1/dooropeners/4",
  "https://edu-iot.iniad.org/api/v1/dooropeners/5",
  "https://edu-iot.iniad.org/api/v1/dooropeners/6",
  "https://edu-iot.iniad.org/api/v1/dooropeners/7",
  "https://edu-iot.iniad.org/api/v1/dooropeners/8"
]

https://edu-iot.iniad.org/api/v1/dooropeners/1
{
  "name": "自動扉DOOROPENER01",
  "item_num": 1,
  "device_type": "dooropener",
  "status": "stopped"
}

https://edu-iot.iniad.org/api/v1/signages
[
  "https://edu-iot.iniad.org/api/v1/signages/1000",
  "https://edu-iot.iniad.org/api/v1/signages/1001"
]

https://edu-iot.iniad.org/api/v1/signages/1000
[
  "https://edu-iot.iniad.org/api/v1/signages/1000/1"
]

https://edu-iot.iniad.org/api/v1/signages/1000/1
{
  "title": "my title",
  "playtime": 30
}

https://edu-iot.iniad.org/api/v1/signages/1000/role
{
  "read": [
    "facilityAdmins",
    "student"
  ],
  "write": [
    "facilityAdmins",
    "student"
  ]
}

$ curl https://edu-iot.iniad.org/api/v1/signages/1000/role -X PUT -d 'read=*' -d write=facilityAdmins,student

```

## ステータスコードと戻り値

- 200
  - GET/PUT/PATCH/DELETE が成功した
    - 結果を返す
  - POST が成功した
    - 作成されたデータを返す
- 400
  - パラメータが不足している
    - どのパラメーターが不足しているか詳細を返す
  - カードの登録可能枚数上限を越えている
    - カード管理 API からのエラー
- 401
  - 認証情報がない、または認証に失敗した
    - 認証情報がパラメータとして渡されていないのか、それともユーザー名・パスワードがエラーなのかを返す
- 403
  - センサーは存在するが、ユーザにはアクセス権が無い
- 404
  - 指定されたパラメータに相当するデータは存在しない
- 409
  - uid (FeliCa IDm) はすでにシステムに登録されているので重複登録できない
    - カード管理 API からのエラー
- 502
  - Google Cloud ロードバランサからのエラー
    - バックエンドとして動作する本 API サーバになんらかの異常がありヘルスチェックに失敗しているときに発生する
    - しばらく待って再試行しても同じエラーが返る場合、運用管理者に依頼して API サーバの再起動をすること
    - システムの構成上、この場合は Error モデルを返すことができない。ロードバランサが提供する通常の HTML ページが返される
- 503
  - サーバーエラー
  - INIAD 学内 LAN ではない IP アドレスからのアクセスの場合
    - 恒久的なエラー
    - 利用しているデバイスが学内 LAN に接続されているかどうかチェックすること
  - 高速アクセスエラー
    - 一時的なエラー
    - 同一ユーザが本 API を高速に呼び出した。同一ユーザがあるリクエストから 100 ms 以内に次のリクエストをおこなった場合
  - ロッカー開錠エラー
    - 一時的なエラー
    - ユビキタスサーバがロッカー開錠に失敗しエラー応答した
    - カード登録・削除によって、ロッカー制御デバイスが ACL 更新と再起動をおこなうために、ロッカー開錠 API が頻繁にエラーになることが予想される。ほとんどの場合、単に再試行すればうまくいく

## API

Parameter の引き渡し方法は GET, DELETE はボディを持てないためクエリパラメータとする。
POST, PUT, PATCH はフォームパラメータまたは JSON とする。

ただし、JSON で渡す場合、リクエストの `Content-Type` ヘッダとして `application/json` を指定しなければならない。

### GET /locker

- Parameter
  - なし
- Response
  - Locker モデル

### POST /locker/open

- Parameter
  - なし
- Response
  - Locker モデル

### GET /pclocker

- Parameter
  - なし
- Response
  - PcLocker モデル

### POST /pclocker/open

- Parameter
  - なし
- Response
  - PcLocker モデル

### GET /smalllocker

- Parameter
  - なし
- Response
  - SmallLocker モデル

### POST /smalllocker/open

- Parameter
  - なし
- Response
  - SmallLocker モデル

### GET /iccards

- Parameter
  - なし
- Response
  - ICCard モデルのリスト（配列）

### POST /iccards/

- Parameter
  - ICCard モデルのうち、下記
    - uid （必須）
    - comment （オプション）
- Response
  - 登録した ICCard モデル（id も含む）

### GET /iccards/{id}

- Parameter
  - id(path 内に存在): iccard の ID（ID はシステム内でユニークではなく、ユーザー内でユニークとする。1 から 5 の数字）
- Response
  - 指定した id を持つ ICCard モデル

学生は 2 枚以上のカード登録が禁止されているため、id は実質的に 1 だけが有効となる。

### PUT/PATCH /iccards/{id}

- Parameter
  - id(path 内に存在): iccard の ID
  - comment(body に存在): コメント
- Response
  - 修正後の ICCard モデル

### DELETE /iccards/{id}

- Parameter
  - id(path 内に存在): iccard の ID
- Response
  - ステータスコード 200
  - 内容: {"message": "ICCard No.#{id} was deleted"}

### GET /sensors/{room_num}

- Parameter
  - room_num(path 内に存在): 部屋番号（4 桁の数字を string で）
  - sensor_type(query 内に存在、オプション): 取得したいセンサータイプ。複数指定する場合はプラス(+)区切りにする。
- Response
  - 指定した部屋に属する Sensor クラスのリスト
  - sensor_type を複数指定した場合、指定されたパラメータの一つでもセンサーが存在している場合は、200 を返して見つかった値のみを返す。
  - 何も結果が無い場合のみ、404 を返す
  - センサーは存在するがリクエストしたユーザにはどのセンサーにもアクセス権が無い場合、403 を返す

### GET /sensors/{room_num}/{sensor_type}

- Parameter
  - room_num(path 内に存在): 部屋番号（4 桁の数字を string で）
  - sensor_type: 取得したいセンサータイプを文字列で指定（Sensor モデルの sensor_type）
- Response
  - 指定した Sensor

## カード登録・削除とロッカー ACL の更新について

カードを登録・削除した場合、その IDm をロッカー ACL に反映させなければ、カードによるロッカー開錠は機能しない。

カード登録(POST /iccards)・削除(DELETE /iccards/{id}) の API が呼び出され、かつリクエストしたユーザにロッカー割り当てがある場合、ロッカー ACL 更新も実施される。

- OpenAM に登録されたカード情報とユビキタスサーバ DB との同期は 1 分毎に実施されているため、1 分以上の待機時間をとる必要がある。
- ユーザからのリクエストによる頻繁すぎるロッカー ACL 更新を避けるため、3 分単位でまとめて更新する。
- ユビキタスサーバのロッカー ACL 更新処理には 10 秒程度以上かかる。
- 更新の必要性を 1 分間隔でチェックしている。

これらの時間を総合して、カード登録・削除 API を呼び出してから、おおむね 5 分程度でロッカー ACL 更新が実施され、ロッカーにカード IDm の登録・削除が反映される。

## デジタルサイネージコンテンツ取得 API

サイネージのコンテンツの設定や取得を行うための API を提供する。

サイネージのコンテンツの設定や取得のアクセス権は、以下の権限グループにより管理される。

- 権限グループ
  - 管理者: facilityAdmins (建物自体の管理者)
  - 専任教員: fullTimeTeachers (専任教員のデフォルトの権限)
  - 非常勤教員: partTimeTeachers (非常勤教員のデフォルトの権限)
  - 学生: students (学生のデフォルトの権限)
  - 職員: officeWorkers (事務職員のデフォルトの権限)
  - ビジター: visitors (ビジターのデフォルトの権限)
  - INIAD 管理: iniadAdmins (スタッフ)
  - サーバ管理: serverAdmins (サーバ管理者の権限)
  - メディアセンター管理: mediaCenterAdmins (メディアセンタースタッフの権限)
  - メイカーズハブ管理: makersHubAdmins (メイカーズハブスタッフの権限)
  - ホール管理: hallAdmins (ホールを管理する人間の権限)
  - 坂村研究室: sakamuraLab (坂村研究室の権限)
  - 清掃員: cleaningWorkers (清掃員)
  - 警備員: securityWorkers (警備員)
  - 大学院教員: graduateSchoolTeachers (大学院授業担当教員の権限)
  - リカレント教育受講生: recurrentEducationStudents (リカレント教育受講生の権限)

以上
