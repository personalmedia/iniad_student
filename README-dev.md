# 開発手順

DBアクセスプロキシサービスが localhost で動いている必要があるので、sshポートフォワードしておく。

```
ssh iniad-api -L 5430:127.0.0.1:5430 -L 5432:127.0.0.1:5432
```

[Leiningen](https://leiningen.org) をインストールしておく。

Emacs Cider のために、プラグインの指示。

% cat ~/.lein/profiles.clj
{:user {:plugins [[cider/cider-nrepl "0.20.0"]]}}

project.clj を開いた状態で、 C-x cider-jack-in すると、しばらくして、 REPL が起動する。

REPL　から
student.server> (run-dev)
とすると、開発サーバーが起動する。

アクセス元チェックがあるので、curl には以下のオプションを追加する。
-H 'x-forwarded-for: 118.238.221.73, 35.190.18.235'


# Tips

Java 17 以降で動作することを確認。

Java 17 は 2029 年までの長期サポートがある。
