#!/bin/sh
echo '"(廃止された開発環境) OpenAM"'
curl https://portal.iniad.org/openam/oauth2/access_token \
     -X POST \
     -u RP_LUXIAR1:pwluxiar123 \
     -d grant_type=password \
     -d scope="openid" \
     -d username=${usarname:-Kazuko.Kawagoe} \
     -d password=${password:-iniadteacherpw} \
     -s | jq


echo '"(廃止された)OpenAM のトークン"'
curl https://accounts.iniad.org/openam/oauth2/access_token \
     -X POST \
     -u iniad-portalapp:gah8Zeis8ohsaefu \
     -d grant_type=password \
     -d scope="openid" \
     -d username=${usarname:-Kazuko.Kawagoe} \
     -d password=${password:-iniadteacherpw} \
     -s | jq


echo '"(開発環境) Keycloak のトークン"'
curl https://portal.iniad.org/keycloak/auth/realms/master/protocol/openid-connect/token \
     -X POST \
     -u iniad-portalapp:d8dbd691-4c65-4e7d-b9c0-736116f97ee8 \
     -d grant_type=password \
     -d scope="openid" \
     -d username=${usarname:-Kazuko.Kawagoe} \
     -d password=${password:-iniadteacherpw} \
     -s | jq


echo '"Keycloak のトークン"'
curl https://login.iniad.org/auth/realms/master/protocol/openid-connect/token \
     -X POST \
     -u iniad-portalapp:pnyqCygnzBLBELJqXoTcpblCj0eHN5kG \
     -d grant_type=password \
     -d scope="openid" \
     -d username=${usarname:-Kazuko.Kawagoe} \
     -d password=${password:-iniadteacherpw} \
     -s | jq
