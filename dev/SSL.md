# SSL (TLS) の設定について

## 適切なバージョンの alpn-boot をダウンロードしてくる。

% curl -LO http://central.maven.org/maven2/org/mortbay/jetty/alpn/alpn-boot/8.1.10.v20161026/alpn-boot-8.1.10.v20161026.jar

何が適切かは、

    http://www.eclipse.org/jetty/documentation/current/alpn-chapter.html#alpn-versions

を見る。

JDKのバージョンによって適用できるバージョンが違うので、バージョンに注意する。


## SSL証明書をパッケージする。

server.key (秘密鍵) と server.crt (証明書) を手元に用意して、以下のコマンドを実行する。

    openssl pkcs12 -inkey server.key -in server.crt -export -out server.pkcs12

src/student/service.clj の最後のところに証明書(pkcs12)のパスワードを設定する。

lein uberjar コマンドを実行すると、アプリのjarファイルが target/ に生成される。

dockerコンテナの作りかたは、他と同様。
